/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transform;

/**
 * Basic Quaternion class for specifying rotations
 * 
 * @author Christopher
 */
public class Quaternion {
    //order [x,y,z,w]
    private final float[] q;
    //[mat] [x]
    //      [y] = rotated point
    //      [z]
    private final float[] mat;
    
    /**
     * Create a Quaternion storing a precomputed quaternion array in x,y,z,w order.
     * x,y,z specify the axis of rotation, while w specified the amount in radians. 
     * Quaternion will be normalized if sufficiently far off magnitude 1.0. 
     * @param q float array to create from
     */
    public Quaternion(float[] q){
        this.q = q;
        //normalize
        float msq = q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3];
        if(msq - 1 > .0001f){
            float invMag = FastFMath.invSqrt(msq);
            q[0]*=invMag;
            q[1]*=invMag;
            q[2]*=invMag;
            q[3]*=invMag;
        }
        this.mat = new float[9];
        mat[0] = 1-2*(q[1]*q[1]+q[2]*q[2]);
        mat[1] = 2*(q[0]*q[1]+q[3]*q[2]);
        mat[2] = 2*(q[0]*q[2]-q[3]*q[1]);
        mat[3] = 2*(q[0]*q[1]-q[3]*q[2]);
        mat[4] = 1-2*(q[0]*q[0]+q[2]*q[2]);
        mat[5] = 2*(q[1]*q[2]+q[3]*q[0]);
        mat[6] = 2*(q[0]*q[2]+q[3]*q[1]);
        mat[7] = 2*(q[1]*q[2]-q[3]*q[0]);
        mat[8] = 1-2*(q[0]*q[0]+q[1]*q[1]);
    }
    
    /**
     * Create a Quaternion storing a precomputed quaternion array in x,y,z,w order.
     * x,y,z specify an axis of rotation with each component * Sin(th/2),
     * while w is Cos(th/2).
     * Quaternion will be normalized if sufficiently far off magnitude 1.0. 
     * @param x x component of axis
     * @param y y component of axis
     * @param z z component of axis
     * @param w angle of rotation in radians
     */
    public Quaternion(float x, float y, float z, float w){
        this.q = new float[] {x,y,z,w};
        //normalize
        float msq = q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3];
        if(msq - 1 > .0001f){
            float invMag = FastFMath.invSqrt(msq);
            q[0]*=invMag;
            q[1]*=invMag;
            q[2]*=invMag;
            q[3]*=invMag;
        }
        this.mat = new float[9];
        mat[0] = 1-2*(q[1]*q[1]+q[2]*q[2]);
        mat[1] = 2*(q[0]*q[1]+q[3]*q[2]);
        mat[2] = 2*(q[0]*q[2]-q[3]*q[1]);
        mat[3] = 2*(q[0]*q[1]-q[3]*q[2]);
        mat[4] = 1-2*(q[0]*q[0]+q[2]*q[2]);
        mat[5] = 2*(q[1]*q[2]+q[3]*q[0]);
        mat[6] = 2*(q[0]*q[2]+q[3]*q[1]);
        mat[7] = 2*(q[1]*q[2]-q[3]*q[0]);
        mat[8] = 1-2*(q[0]*q[0]+q[1]*q[1]);
    }
    
    /**
     * Create a quaternion with {@code a} axis of rotation and {@code angle} angle of rotation
     * @param a 3-float array specifying axis of rotation; should be a unit vector
     * @param angle angle of rotation in radians
     */
    public Quaternion(float[] a, double angle){
        this.q = new float[4];
        this.q[3] = (float)Math.cos(angle/2.0);
        float mult = (float)Math.sin(angle/2.0);
        this.q[0] = mult*a[0];
        this.q[1] = mult*a[1];
        this.q[2] = mult*a[2];
        float msq = q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3];
        if(msq - 1 > .0001f){
            float invMag = FastFMath.invSqrt(msq);
            q[0]*=invMag;
            q[1]*=invMag;
            q[2]*=invMag;
            q[3]*=invMag;
        }
        this.mat = new float[9];
        mat[0] = 1-2*(q[1]*q[1]+q[2]*q[2]);
        mat[1] = 2*(q[0]*q[1]+q[3]*q[2]);
        mat[2] = 2*(q[0]*q[2]-q[3]*q[1]);
        mat[3] = 2*(q[0]*q[1]-q[3]*q[2]);
        mat[4] = 1-2*(q[0]*q[0]+q[2]*q[2]);
        mat[5] = 2*(q[1]*q[2]+q[3]*q[0]);
        mat[6] = 2*(q[0]*q[2]+q[3]*q[1]);
        mat[7] = 2*(q[1]*q[2]-q[3]*q[0]);
        mat[8] = 1-2*(q[0]*q[0]+q[1]*q[1]);
    }
    
    /**
     * Compute quaternion product q1*q2 (note, not commutative)
     * 
     * @param q1 quaternion to left of multiplication
     * @param q2 quaternion to right of multiplication
     * @return new Quaternion object storing the result
     */
    public static Quaternion qMultiply(Quaternion q1, Quaternion q2){
        return new Quaternion(new float[] {
            q1.q[3]*q2.q[0]+q1.q[0]*q2.q[3]+q1.q[1]*q2.q[2]-q1.q[2]*q2.q[1],
            q1.q[3]*q2.q[1]-q1.q[0]*q2.q[2]+q1.q[1]*q2.q[3]+q1.q[2]*q2.q[0],
            q1.q[3]*q2.q[2]+q1.q[0]*q2.q[1]-q1.q[1]*q2.q[0]-q1.q[2]*q2.q[3],
            q1.q[3]*q2.q[3]-q1.q[0]*q2.q[0]-q1.q[1]*q2.q[1]-q1.q[2]*q2.q[2]
        });
    }
    
    public static void qMultiply(float[] q1, float[] q2,float[] out){
        out[0] = q1[3]*q2[0]+q1[0]*q2[3]+q1[1]*q2[2]-q1[2]*q2[1];
        out[1] = q1[3]*q2[1]-q1[0]*q2[2]+q1[1]*q2[3]+q1[2]*q2[0];
        out[2] = q1[3]*q2[2]+q1[0]*q2[1]-q1[1]*q2[0]-q1[2]*q2[3];
        out[3] = q1[3]*q2[3]-q1[0]*q2[0]-q1[1]*q2[1]-q1[2]*q2[2];
    }
    
    
    /**
     * performs q2 * receiver object
     * @param q2 quat float array to left multiply by in order [x,y,z,w]
     * @param temp scratch work array
     */
    public void multAcc(float[] q2,float[] temp){
        temp[0] = q2[3]*q[0]+q2[0]*q[3]-q2[1]*q[2]+q2[2]*q[1];
        temp[1] = q2[3]*q[1]+q2[0]*q[2]+q2[1]*q[3]-q2[2]*q[0];
        temp[2] = q2[3]*q[2]-q2[0]*q[1]+q2[1]*q[0]+q2[2]*q[3];
        temp[3] = q2[3]*q[3]-q2[0]*q[0]-q2[1]*q[1]-q2[2]*q[2];
        q[0] = temp[0];
        q[1] = temp[1];
        q[2] = temp[2];
        q[3] = temp[3];
    }
    
    /**
     * Normalize this quaternion if error is exceeded
     * @param err Will normalize this quaternion if magnitude^2-1 &gt; err
     */
    public void normIfErr(float err){
        float msq = q[0]*q[0]+q[1]*q[1]+q[2]*q[2]+q[3]*q[3];
        if(msq - 1 > err){
            float invMag = FastFMath.invSqrt(msq);
            q[0]*=invMag;
            q[1]*=invMag;
            q[2]*=invMag;
            q[3]*=invMag;
        }
    }
    
    /**
     * recompute this quaternion's rotation matrix
     */
    public void genMat(){
        mat[0] = 1-2*(q[1]*q[1]+q[2]*q[2]);
        mat[1] = 2*(q[0]*q[1]+q[3]*q[2]);
        mat[2] = 2*(q[0]*q[2]-q[3]*q[1]);
        mat[3] = 2*(q[0]*q[1]-q[3]*q[2]);
        mat[4] = 1-2*(q[0]*q[0]+q[2]*q[2]);
        mat[5] = 2*(q[1]*q[2]+q[3]*q[0]);
        mat[6] = 2*(q[0]*q[2]+q[3]*q[1]);
        mat[7] = 2*(q[1]*q[2]-q[3]*q[0]);
        mat[8] = 1-2*(q[0]*q[0]+q[1]*q[1]);
    }
    
    /**
     * Rotate the given 3D point about the origin and store the result in dest
     * @param source point to rotate
     * @param dest 3-float array to store result in
     */
    public void rotatePt(float[] source, float[] dest){
        dest[0] = source[0]*mat[0]+source[1]*mat[1]+source[2]*mat[2];
        dest[1] = source[0]*mat[3]+source[1]*mat[4]+source[2]*mat[5];
        dest[2] = source[0]*mat[6]+source[1]*mat[7]+source[2]*mat[8];
    }
    
    /**
     * Rotate the given 3D point about the origin and store the result in dest
     * @param source point to rotate
     * @param mat matrix to perform rotation with
     * @param dest 3-float array to store result in
     */
    public static void rotatePtStat(float[] source, float[] mat,float[] dest){
        dest[0] = source[0]*mat[0]+source[1]*mat[1]+source[2]*mat[2];
        dest[1] = source[0]*mat[3]+source[1]*mat[4]+source[2]*mat[5];
        dest[2] = source[0]*mat[6]+source[1]*mat[7]+source[2]*mat[8];
    }
    
    /**
     * copy the value of this quaternion to an array
     * @param dest array to copy to
     */
    public void copyArr(float[] dest){
        dest[0] = q[0];
        dest[1] = q[1];
        dest[2] = q[2];
        dest[3] = q[3];
    }
    
    @Override
    public String toString(){
        return ("[" + Float.toString(q[0]) + ", " + Float.toString(q[1]) + ", " + 
                Float.toString(q[2]) + ", " + Float.toString(q[3]) + "]" );
    }
}
