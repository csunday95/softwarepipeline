/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package transform;

/**
 *
 * @author Christopher
 */
public abstract class FastFMath {
        
    /**
     * An implementation of the fast iterative square root method to be used for
     * real time vector normalization. Magic numbers anyone? 
     * 
     * @param x the float value that will have its inverse square root calculated
     * @return the float inverse square root of x
     * @see <a href="http://stackoverflow.com/questions/11513344/how-to-implement-the-fast-inverse-square-root-in-java">Stack Overflow credit</a>
     */
    public static float invSqrt(float x) {
        float xhalf = 0.5f*x;
        int i = Float.floatToIntBits(x);
        i = 0x5f3759df - (i>>1);
        x = Float.intBitsToFloat(i);
        x = x*(1.5f - xhalf*x*x);
        return x;
    }
    
    /**
     * A utility method that normalizes in place a 3D vector represented by a float[].
     * 
     * @param in a 3 float array that specifies a vector to be normalized 
     */
    public static void normalizeVec(float[] in){
        float invMag = invSqrt(in[0]*in[0]+in[1]*in[1]+in[2]*in[2]);
        in[0]*=invMag;
        in[1]*=invMag;
        in[2]*=invMag;
    }
    
    public static void computeNormal(float[] tri, float[] n){
        float vx,vy,vz;
        float wx,wy,wz;
        vx = tri[6]-tri[0];
        vy = tri[7]-tri[1];
        vz = tri[8]-tri[2];
        wx = tri[3]-tri[0];
        wy = tri[4]-tri[1];
        wz = tri[5]-tri[2];
        n[0] = vy*wz-vz*wy;
        n[1] = vz*wx-vx*wz;
        n[2] = vx*wy-vy*wx;
        FastFMath.normalizeVec(n);
    }
}
