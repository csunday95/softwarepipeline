/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import transform.FastFMath;
import transform.Quaternion;

/**
 * Utility class for perspective projection/ other camera activities 
 * @author Christopher
 */
public class PerspectiveProject {

    private final float projDist;
    private final int screenSize;
    private final float[] camLoc;
    private final float[] accumulatedTranslation;
    private final double[] accumulatedRotation;
    public double[] viewAng;
    
    //a normalized view direction vector
    public float[] viewDir;
    
    //a normalized vector representing the up direction
    public float[] upDir;
    
    //a normalized vector representing the right direction
    public float[] rightDir;
    
    //current camera rotation quaternion
    private final float[] rotationQ;
    
    //matrix form of quaternion
    private final float[] quatMat;
    
    //temp matrix for computing rotation accumulation
    private final float[] quatRotTemp;
    
    //temp matrix for accumulating translations
    private final float[][] transTemp;
    
    //temp quaternion
    private final float[] q2;
    
    //scratch space for accumulating quaternion 
    private final float[] qTemp;
    
    
    /**
     * Perspective projection utility class. Handles all of the camera 
     * information and projection functions. 
     * @param distToPlane distance projection plane is from camera.
     * @param cameraLoc initial location of the camera
     * @param viewAngles initial camera angle relative to +z axis
     *                   specified by yaw,pitch,roll in radians 
     * @param screenSize size of screen projection (e.g. 480 pixels)
     */
    public PerspectiveProject(float distToPlane, float[] cameraLoc, double[] viewAngles,int screenSize) {
        this.projDist = distToPlane;
        this.camLoc = cameraLoc;
        this.viewAng = viewAngles;
        this.rotationQ = new float[4];
        float[] temp = new float[4];
        //create rotation Quaternion and input inital angle
        Quaternion q = new Quaternion((float)Math.sin(viewAngles[0]/2),0,0,
                                       (float)Math.cos(viewAngles[0]/2));
        q.multAcc(new float[] {0,(float)Math.sin(viewAngles[1]/2),0,
                               (float)Math.cos(viewAngles[1]/2)}, temp);
        q.multAcc(new float[] {0,0,(float)Math.sin(viewAngles[2]/2),
                               (float)Math.cos(viewAngles[2]/2)}, temp);
        q.copyArr(this.rotationQ);
        this.q2 = new float[4];
        this.qTemp = new float[4];
        this.quatMat = new float[9];
        this.quatRotTemp = new float[3];
        this.viewDir = new float[3];
        this.upDir = new float[3];
        this.rightDir = new float[3];
        this.recalculateViewDir();
        this.accumulatedRotation = new double[]{0, 0, 0};
        this.accumulatedTranslation = new float[]{0, 0, 0};
        this.transTemp = new float[2][3];
        this.screenSize = screenSize;
    }
    
    /**
     * Recomputes the view, right and up directions as well as recomputing
     * the rotation matrix and normalizing the quaternion if necessary (due to 
     * drift). 
     *
     */
    public final void recalculateViewDir() {
        //normalize right dir so a normalized up dir can be computed
        normIfErr(0.000005f);
        quatMat[0] = 1-2*(rotationQ[1]*rotationQ[1]+rotationQ[2]*rotationQ[2]);
        quatMat[1] = 2*(rotationQ[0]*rotationQ[1]+rotationQ[3]*rotationQ[2]);
        quatMat[2] = 2*(rotationQ[0]*rotationQ[2]-rotationQ[3]*rotationQ[1]);
        quatMat[3] = 2*(rotationQ[0]*rotationQ[1]-rotationQ[3]*rotationQ[2]);
        quatMat[4] = 1-2*(rotationQ[0]*rotationQ[0]+rotationQ[2]*rotationQ[2]);
        quatMat[5] = 2*(rotationQ[1]*rotationQ[2]+rotationQ[3]*rotationQ[0]);
        quatMat[6] = 2*(rotationQ[0]*rotationQ[2]+rotationQ[3]*rotationQ[1]);
        quatMat[7] = 2*(rotationQ[1]*rotationQ[2]-rotationQ[3]*rotationQ[0]);
        quatMat[8] = 1-2*(rotationQ[0]*rotationQ[0]+rotationQ[1]*rotationQ[1]);
        //multiply unit vectors by conjugate quaternion (axis direction inverted)
        this.viewDir[0] = 2*(rotationQ[0]*rotationQ[2]+rotationQ[3]*rotationQ[1]);
        this.viewDir[1] = 2*(rotationQ[1]*rotationQ[2]-rotationQ[3]*rotationQ[0]);
        //diagonal is identical for conjugate quaternion
        this.viewDir[2] = quatMat[8];
        this.upDir[0] = 2*(rotationQ[0]*rotationQ[1]-rotationQ[3]*rotationQ[2]);
        this.upDir[1] = quatMat[4];
        this.upDir[2] = 2*(rotationQ[1]*rotationQ[2]+rotationQ[3]*rotationQ[0]);
        this.rightDir[0] = quatMat[0];
        this.rightDir[1] = 2*(rotationQ[0]*rotationQ[1]+rotationQ[3]*rotationQ[2]);
        this.rightDir[2] = 2*(rotationQ[0]*rotationQ[2]-rotationQ[3]*rotationQ[1]);
        //interestingly, the 3x3 matrix with -right,up,view as column vectors
        //is now the conjugate rotation matrix
    }
    
    /**
     * Normalize rotation quaternion if deviation from unit vector too high
     * @param err max value for mag^2 before renormalizing
     */
    private void normIfErr(float err){
        float msq = rotationQ[0]*rotationQ[0]+rotationQ[1]*rotationQ[1]+
                    rotationQ[2]*rotationQ[2]+rotationQ[3]*rotationQ[3];
        if(msq - 1 > err){
            float invMag = FastFMath.invSqrt(msq);
            rotationQ[0]*=invMag;
            rotationQ[1]*=invMag;
            rotationQ[2]*=invMag;
            rotationQ[3]*=invMag;
        }
    }
    
    /**
     * transforms 3 points in world space to screen space. The points are specified
     * by a float array that gives coordinates relative to the world
     * space origin.
     *
     * @param in triangle as a 9-float array 
     * @param out float array to write to, length 6
     * @see
     * <a href = "http://en.wikipedia.org/wiki/3d_projection#Perspective_projection">Wikipedia
     * entry on 3D projection</a>
    **/
    public void pointProject3(float[] in, float[] out) {
        //calculate relative coordinate
        float x = in[0] - camLoc[0];
        float y = in[1] - camLoc[1];
        float z = in[2] - camLoc[2];
        //scaling factor based on distance 
        quatRotTemp[0] = x*quatMat[0]+y*quatMat[1]+z*quatMat[2];
        quatRotTemp[1] = x*quatMat[3]+y*quatMat[4]+z*quatMat[5];
        quatRotTemp[2] = x*quatMat[6]+y*quatMat[7]+z*quatMat[8];
        float scaleFactor = this.projDist/quatRotTemp[2];
        out[0] = this.screenSize*scaleFactor*quatRotTemp[0];
        out[1] = this.screenSize*scaleFactor*quatRotTemp[1];
        //calculate relative coordinate
        x = in[3] - camLoc[0];
        y = in[4] - camLoc[1];
        z = in[5] - camLoc[2];
        //scaling factor based on distance 
        quatRotTemp[0] = x*quatMat[0]+y*quatMat[1]+z*quatMat[2];
        quatRotTemp[1] = x*quatMat[3]+y*quatMat[4]+z*quatMat[5];
        quatRotTemp[2] = x*quatMat[6]+y*quatMat[7]+z*quatMat[8];
        scaleFactor = this.projDist/quatRotTemp[2];
        out[2] = this.screenSize*scaleFactor*quatRotTemp[0];
        out[3] = this.screenSize*scaleFactor*quatRotTemp[1];
        //calculate relative coordinate
        x = in[6] - camLoc[0];
        y = in[7] - camLoc[1];
        z = in[8] - camLoc[2];
        //scaling factor based on distance 
        quatRotTemp[0] = x*quatMat[0]+y*quatMat[1]+z*quatMat[2];
        quatRotTemp[1] = x*quatMat[3]+y*quatMat[4]+z*quatMat[5];
        quatRotTemp[2] = x*quatMat[6]+y*quatMat[7]+z*quatMat[8];
        scaleFactor = this.projDist/quatRotTemp[2];
        out[4] = this.screenSize*scaleFactor*quatRotTemp[0];
        out[5] = this.screenSize*scaleFactor*quatRotTemp[1];
    }
    
    /**
     * transforms a point in world space to screen space. The point is specified
     * by a float array that gives coordinates relative to the world
     * space origin.
     *
     * @param xin x coordinate
     * @param yin y coordinate
     * @param zin z coordinate
     * @param out float array to write to
     * @param off offset in array to write to
     * @see
     * <a href = "http://en.wikipedia.org/wiki/3d_projection#Perspective_projection">Wikipedia
     * entry on 3D projection</a>
    **/
    public void pointProjectSingle(float xin, float yin, float zin, float[] out,int off) {
        //calculate relative coordinate
        float x = xin - camLoc[0];
        float y = yin - camLoc[1];
        float z = zin - camLoc[2];
        //scaling factor based on distance 
        quatRotTemp[0] = x*quatMat[0]+y*quatMat[1]+z*quatMat[2];
        quatRotTemp[1] = x*quatMat[3]+y*quatMat[4]+z*quatMat[5];
        quatRotTemp[2] = x*quatMat[6]+y*quatMat[7]+z*quatMat[8];
        float scaleFactor = this.projDist/quatRotTemp[2];
        out[off] = this.screenSize*scaleFactor*quatRotTemp[0];
        out[off+1] = this.screenSize*scaleFactor*quatRotTemp[1];
    }
    
    /**
     * adds a translation to the queue to be implemented next time the camera
     * is ready to be moved.
     * 
     * @param translation the float array translation in the x-y-z directions
     */
    public void queueTrans(float[] translation){
        this.accumulatedTranslation[0]+=translation[0];
        this.accumulatedTranslation[1]+=translation[1];
        this.accumulatedTranslation[2]+=translation[2];
    }
    
    /**
     * adds a translation to the queue to be implemented next time the camera
     * is ready to be moved.
     * 
     * @param rotation the double array rotation using tait brayn angles
     */
    public void queueRot(double[] rotation){
        this.accumulatedRotation[0]+=rotation[0];
        this.accumulatedRotation[1]+=rotation[1];
        this.accumulatedRotation[2]+=rotation[2];
    }
    
     /**
     * a method to be called to tell the perspective transform its free to move
     * /rotate the camera as the image is now drawn
     * @return an int array with the translation queue 
     */
    public float[][] implementQueues(){
        //don't use this
        return null;
    }
    
    /**
     * @return world space camera location
     */
    public float[] camLoc(){
        return this.camLoc;
    }
    
    /**
     * @return view direction unit vector
     */
    public float[] viewDir(){
        return this.viewDir;
    }
    
    /**
     * @return right direction unit vector (currently bugged)
     */
    public float[] rightDir(){
        return this.rightDir;
    }
    
    /**
     * @return up direction unit vector (currently bugged)
     */
    public float[] upDir(){
        return this.upDir;
    }
    
    protected void setCamPos(float x, float y, float z){
        this.camLoc[0] = x;
        this.camLoc[1] = y;
        this.camLoc[2] = z;
    }
    
    protected void setCamPos(float[] to){
        this.camLoc[0] = to[0];
        this.camLoc[1] = to[1];
        this.camLoc[2] = to[2];
    }
    
    protected void moveCamBy(float x, float y, float z){
        this.camLoc[0]+=x;
        this.camLoc[1]+=y;
        this.camLoc[2]+=z;
    }
    
    protected void moveCamBy(float[] by){
        this.camLoc[0]+=by[0];
        this.camLoc[1]+=by[1];
        this.camLoc[2]+=by[2];
    }
    
    protected void setCameraAngle(double ax, double ay, double az){
        this.rotationQ[0] = 0;
        this.rotationQ[1] = 0;
        this.rotationQ[2] = 0;
        this.rotationQ[3] = 1;
        this.rotateCameraBy(ax, ay, az);
    }
    
    protected void setCameraAngle(double[] a){
        this.setCameraAngle(a[0], a[1], a[2]);
    }
    
    protected void rotateCameraBy(double ax, double ay, double az){
        q2[0] = (float)Math.sin(ax/2);
        q2[1] = 0;
        q2[2] = 0;
        q2[3] = (float)Math.cos(ax/2);
        //q2[1] and q2[2] terms can be excluded
        qTemp[0] = q2[3]*rotationQ[0]+q2[0]*rotationQ[3];
        qTemp[1] = q2[3]*rotationQ[1]+q2[0]*rotationQ[2];
        qTemp[2] = q2[3]*rotationQ[2]-q2[0]*rotationQ[1];
        qTemp[3] = q2[3]*rotationQ[3]-q2[0]*rotationQ[0];
        rotationQ[0] = qTemp[0];
        rotationQ[1] = qTemp[1];
        rotationQ[2] = qTemp[2];
        rotationQ[3] = qTemp[3];
        q2[0] = 0;
        q2[1] = (float)Math.sin(ay/2);
        q2[2] = 0;
        q2[3] = (float)Math.cos(ay/2);
        //q2[0] and q2[2] terms exlcuded
        qTemp[0] = q2[3]*rotationQ[0]-q2[1]*rotationQ[2];
        qTemp[1] = q2[3]*rotationQ[1]+q2[1]*rotationQ[3];
        qTemp[2] = q2[3]*rotationQ[2]+q2[1]*rotationQ[0];
        qTemp[3] = q2[3]*rotationQ[3]-q2[1]*rotationQ[1];
        rotationQ[0] = qTemp[0];
        rotationQ[1] = qTemp[1];
        rotationQ[2] = qTemp[2];
        rotationQ[3] = qTemp[3];
        q2[0] = 0;
        q2[1] = 0;
        q2[2] = (float)Math.sin(az/2);
        q2[3] = (float)Math.cos(az/2);
        //exclude 0 and 1
        qTemp[0] = q2[3]*rotationQ[0]+q2[2]*rotationQ[1];
        qTemp[1] = q2[3]*rotationQ[1]-q2[2]*rotationQ[0];
        qTemp[2] = q2[3]*rotationQ[2]+q2[2]*rotationQ[3];
        qTemp[3] = q2[3]*rotationQ[3]-q2[2]*rotationQ[2];
        rotationQ[0] = qTemp[0];
        rotationQ[1] = qTemp[1];
        rotationQ[2] = qTemp[2];
        rotationQ[3] = qTemp[3];
    }
    
    protected void rotateCameraBy(double[] a){
        this.rotateCameraBy(a[0], a[1], a[2]);
    }
    
    /**
     * rotates a point by the current orientation angles
     * 
     * @param ptIn point to rotate
     * @param ptOut output array
     */
    protected void inverseRotatePoint(float[] ptIn,float[] ptOut){
        ptOut[0] = rightDir[0]*ptIn[0]+upDir[0]*ptIn[1]+viewDir[0]*ptIn[2];
        ptOut[1] = rightDir[1]*ptIn[0]+upDir[1]*ptIn[1]+viewDir[1]*ptIn[2];
        ptOut[2] = rightDir[2]*ptIn[0]+upDir[2]*ptIn[1]+viewDir[2]*ptIn[2];
    }
}
