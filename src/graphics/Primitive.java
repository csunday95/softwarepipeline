/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import transform.Quaternion;

/**
 * Base class for world space primitive geometries. 
 * 
 * @author Christopher
 */
public abstract class Primitive {
    
    private boolean isVisible = true;
    
    //world space coordinate list
    abstract float[][][] asTrianglePoints();
    
    //texture space coordinate list
    abstract float[][][] triangleTexturePoints();
    
    //texture indices
    abstract int[] textureNumbers();
    
    abstract void swapTexture(int newTexId);
    
    //translate primitive
    abstract void translate(float[] v);
    
    //rotate primitive about world space origin using rotation
    abstract void rotate(Quaternion rotation,float[] temp);
    
    //rotate primitive about center using rotation 
    abstract void pointRotate(Quaternion rotation, float[] center, float[] temp);
    
    abstract float[] getNormal();
    
    public abstract boolean rayTraceIntersect(float[] direction, float[] origin);
    
    public void setVisible(boolean setTo){
        this.isVisible = setTo;
    }
    
    public boolean isVisible(){
        return this.isVisible;
    }
}
