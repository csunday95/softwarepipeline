/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.util.ArrayList;

/**
 * First stab at a BSP tree data structure for static environments
 * 
 * 
 * @author Christopher
 */

public class BSPTree{
    
    private final Node root;
    private final ArrayList<Primitive> traversalList;
    private int size;
    
    public BSPTree(Primitive init){
        this.root = new Node(init);
        this.traversalList = new ArrayList<>();
        this.size = -1;
    }
    
    //empty bsp tree place holder
    public BSPTree(){
        this.root = null;
        this.traversalList = new ArrayList<>();
        this.size = -1;
    }
    
    public void build(ArrayList<Primitive> contents){
        if(this.root!=null) this.root.addPrimatives(contents);
        traversalList.clear();
        this.root.nearestFarthestTraverse(new float[] {0,0,0},traversalList);
        this.size = traversalList.size();
        traversalList.clear();
    }
    
    public int size(){
        System.out.println(this.size);
        return this.size;
    }
    
    //really inefficient, move over to dynamically computed iterator model later
    public Iterable<Primitive> nearestFarthestTraversal(float[] camPos) {
        if(this.root==null) return this.traversalList;
        this.traversalList.clear();
        this.root.nearestFarthestTraverse(camPos, traversalList);
        //System.out.println(this.traversalList.size());
        return this.traversalList;
        //return new BSPIterator(camPos,this.root,this.traversalList);
    }
    
    private static class Node{
        enum RelPosition {FRONT, BEHIND, INTERSECT, INPLANE};
        private final ArrayList<Primitive> inPlaneList;
        private final float[] plnPt;
        private final float[] normal;
        private Node front;
        private Node behind;
        private final ArrayList<Primitive> frontList;
        private final ArrayList<Primitive> behindList;
        
        public Node(Primitive p){
            this.inPlaneList = new ArrayList<>();
            this.inPlaneList.add(p);
            this.plnPt = p.asTrianglePoints()[0][0];
            this.normal = p.getNormal();
            this.frontList = new ArrayList<>();
            this.behindList = new ArrayList<>();
            this.front = null;
            this.behind = null;
        }
        
        public RelPosition ptInFront(float[] pt){
            float dot =  (pt[0]-plnPt[0])*normal[0] + 
                         (pt[1]-plnPt[1])*normal[1] + 
                         (pt[2]-plnPt[2])*normal[2];
            if(dot > 0){
                return RelPosition.FRONT;
            }
            else if(dot < 0){
                return RelPosition.BEHIND;
            }
            return RelPosition.INPLANE;
        }
        
        public void nearestFarthestTraverse(float[] camPos, ArrayList<Primitive> travList){
            if(this.isLeaf()){
                travList.addAll(this.inPlaneList);
                return;
            }
            switch(this.ptInFront(camPos)){
                case FRONT:
                    if(this.front != null) this.front.nearestFarthestTraverse(camPos, travList);
                    travList.addAll(this.inPlaneList);
                    if(this.behind != null)this.behind.nearestFarthestTraverse(camPos, travList);
                    break;
                case BEHIND:
                    if(this.behind != null) this.behind.nearestFarthestTraverse(camPos, travList);
                    travList.addAll(this.inPlaneList);
                    if(this.front != null)this.front.nearestFarthestTraverse(camPos, travList);
                    break;
                case INPLANE:
                    if(this.front != null)this.front.nearestFarthestTraverse(camPos, travList);
                    travList.addAll(this.inPlaneList);
                    if(this.behind != null)this.behind.nearestFarthestTraverse(camPos, travList);
            }
        }
        
        public void addPrimatives(ArrayList<Primitive> polys){
            RelPosition pos;
            float[][][] triPts,texPts;
            int[] texNums;
            for(Primitive p : polys){
                triPts = p.asTrianglePoints();
                texPts = p.triangleTexturePoints();
                texNums = p.textureNumbers();
                for(int i = 0; i < triPts.length; i++){
                    pos = this.localize(triPts[i], this.plnPt, this.normal);
                    Triangle toAdd;
                    switch(pos){
                        case FRONT:
                            toAdd = new Triangle(triPts[i],texPts[i],texNums[i]);
                            this.frontList.add(toAdd);
                            break;
                        case BEHIND:
                            toAdd = new Triangle(triPts[i],texPts[i],texNums[i]);
                            this.behindList.add(toAdd);
                            break;
                        case INTERSECT:
                            Primitive[] sub = 
                                    this.subdivide(triPts[i], texPts[i], texNums[i], plnPt, normal);
                            this.frontList.add(sub[0]);
                            this.behindList.add(sub[1]);
                            break;
                        case INPLANE:
                            toAdd = new Triangle(triPts[i],texPts[i],texNums[i]);
                            this.inPlaneList.add(toAdd);
                    }
                }
            }
            if(!this.frontList.isEmpty()){
                if(this.front == null){
                    this.front = new Node(this.frontList.remove(this.frontList.size()-1));
                }
                this.front.addPrimatives(this.frontList);
            }
            if(!this.behindList.isEmpty()){
                if(this.behind == null){
                    this.behind = new Node(this.behindList.remove(this.behindList.size()-1));
                }
                this.behind.addPrimatives(this.behindList);
            }
            //try to minimize dynamic memory use a bit
            this.frontList.clear();
            this.frontList.trimToSize();
            this.behindList.clear();
            this.behindList.trimToSize();
        }
        
        /**
         * returns a RelPosition value indicating the location of the given
         * triangle {@code tri} relative to the plane specified by {@code plnpt} and 
         * {@code normal}
         * 
         * @param tri 2d float array of length 3x3 specifying 3 vertices
         * @param plnpt point on the test plane
         * @param normal oriented normal of the test plane
         * @return RelPosition value corresponding to the spatial configuration
         */
        private RelPosition localize(float[][] tri,float[] plnpt, float[] normal){
            int frontCount = 0;
            int inCount = 0;
            float dot;
            for(int i = 0; i < 3; i++){
                dot = (tri[i][0]-plnpt[0])*normal[0]+
                      (tri[i][1]-plnpt[1])*normal[1]+
                      (tri[i][2]-plnpt[2])*normal[2];
                if (dot >= 0){
                    frontCount++;
                    if(dot==0){
                        inCount++;
                    }
                }
                else{
                    frontCount--;
                }
            }
            if(frontCount == 3){
                if(inCount==3) return RelPosition.INPLANE;
                return RelPosition.FRONT;
            }
            if (frontCount == -3){
                return RelPosition.BEHIND;
            }
            return RelPosition.INTERSECT;
        }
        
        /**
         * divides a given triangle into a triangle and a quad as it is split 
         * by the plane specified by {@code plnPt} and {@code normal}.
         * 
         * @param tri a 3x3 float array specifying triangle vertices
         * @param texPts a 3x3 float array specifying corresponding texture vertices
         * @param texNum texture Id for this triangle
         * @param plnPt 3 float array specifying a point on the plane in question
         * @param normal 3 float array specifying oriented normal of plane
         * @return an array containing a Triangle and a Quad, with the one
         *         in "front" of the given plane first
         */
        private Primitive[] subdivide(float[][] tri, float[][] texPts, int texNum,
                                      float[] plnPt, float[] normal){
            Primitive[] ret = new Primitive[2];
            int minorityIdx;
            boolean firstFront,secondFront,triInFront;
            firstFront = (tri[0][0]-plnPt[0])*normal[0]+
                         (tri[0][1]-plnPt[1])*normal[1]+
                         (tri[0][2]-plnPt[2])*normal[2] >= 0;
            secondFront = (tri[1][0]-plnPt[0])*normal[0]+
                          (tri[1][1]-plnPt[1])*normal[1]+
                          (tri[1][2]-plnPt[2])*normal[2] >= 0;
            if(firstFront == secondFront){
                minorityIdx = 2;
                triInFront = !firstFront;
            }
            else if((tri[2][0]-plnPt[0])*normal[0]+
                    (tri[2][1]-plnPt[1])*normal[1]+
                    (tri[2][2]-plnPt[2])*normal[2] >= 0){
                if(firstFront) minorityIdx = 1;
                else minorityIdx = 0;
                triInFront = false;
            }
            else{
                if(firstFront) minorityIdx = 0;
                else minorityIdx = 1;
                triInFront = true;
            }
            //find two points of intersection with plane
            float[] i1 = new float[3];
            float[] i2 = new float[3];
            float[] texI1 = new float[2];
            float[] texI2 = new float[2];
            int idx1 = (minorityIdx+1)%3;
            int idx2 = (minorityIdx+2)%3;
            float[] v = new float[] {tri[idx1][0]-tri[minorityIdx][0],
                                     tri[idx1][1]-tri[minorityIdx][1],
                                     tri[idx1][2]-tri[minorityIdx][2]};
            float[] texV = new float[] {texPts[idx1][0]-texPts[minorityIdx][0],
                                        texPts[idx1][1]-texPts[minorityIdx][1]};
            //compute value of line parameter where intersection occurs
            float t = normal[0]*(plnPt[0]-tri[minorityIdx][0]) + 
                      normal[1]*(plnPt[1]-tri[minorityIdx][1]) + 
                      normal[2]*(plnPt[2]-tri[minorityIdx][2]);
            t/= normal[0]*(v[0]) + 
                normal[1]*(v[1]) + 
                normal[2]*(v[2]);
            i1[0] = tri[minorityIdx][0] + t*v[0];
            i1[1] = tri[minorityIdx][1] + t*v[1];
            i1[2] = tri[minorityIdx][2] + t*v[2];
            texI1[0] = texPts[minorityIdx][0] + t*texV[0];
            texI1[1] = texPts[minorityIdx][1] + t*texV[1];
            v[0] = tri[idx2][0] - tri[minorityIdx][0];
            v[1] = tri[idx2][1] - tri[minorityIdx][1];
            v[2] = tri[idx2][2] - tri[minorityIdx][2];
            texV[0] = texPts[idx2][0]-texPts[minorityIdx][0];
            texV[1] = texPts[idx2][1]-texPts[minorityIdx][1];
            t = normal[0]*(plnPt[0]-tri[minorityIdx][0]) + 
                normal[1]*(plnPt[1]-tri[minorityIdx][1]) + 
                normal[2]*(plnPt[2]-tri[minorityIdx][2]);
            t/= normal[0]*(v[0]) + 
                normal[1]*(v[1]) + 
                normal[2]*(v[2]);
            i2[0] = tri[minorityIdx][0] + t*v[0];
            i2[1] = tri[minorityIdx][1] + t*v[1];
            i2[2] = tri[minorityIdx][2] + t*v[2];
            texI2[0] = texPts[minorityIdx][0] + t*texV[0];
            texI2[1] = texPts[minorityIdx][1] + t*texV[1];
            //later on will have to figure out how to determine winding order
            //to keep normals oriented the same; for now it's ambiguous
            int idx = (triInFront) ? 0 : 1;
            ret[idx] = new Triangle(tri[minorityIdx],texPts[minorityIdx],
                                  i1,texI1,i2,texI2,texNum);
            ret[(idx+1)%2] = new Quad(tri[idx1],texPts[idx1],tri[idx2],texPts[idx2],
                              i2,texI2,i1,texI1,texNum);
            return ret;
        }
        
        public boolean isLeaf(){
            return (this.front == null && this.behind == null);
        }
    }
}
