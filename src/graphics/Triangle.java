/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.util.Arrays;
import transform.FastFMath;
import transform.Quaternion;

/**
 * Single triangle primitive
 * 
 * @author Christopher
 */
public class Triangle extends Primitive{
    
    private final float[][][] points;
    private final int[] textureNums;
    private final float[][][] texturePts;
    private float[] normal;
    
    /**
     * Create a triangle primitive. points specified in counterclockwise order. 
     * 
     * @param a 3D point A
     * @param texA texture point A
     * @param b 3D point A
     * @param texB texture point B
     * @param c 3D point A
     * @param texC texture point C
     * @param texNum texture registered for use by this triangle 
     */
    public Triangle(float[] a, float[] texA, float[] b, float[] texB, float[] c,
            float[] texC, int texNum){
        this.points = new float[][][] {{a,b,c},};
        this.textureNums = new int[] {texNum};
        this.texturePts = new float[][][] {{texA,texB,texC},};
        this.normal = null;
    }
    
    /**
     * Construct a Triangle from vertex arrays
     * 
     * @param vertices 3x3 array of vertices
     * @param texVertices 3x2 array of texture points corresponding to vertices
     * @param texNum texture id
     */
    public Triangle(float[][] vertices, float[][] texVertices, int texNum){
        this(vertices[0],texVertices[0],vertices[1],texVertices[1],
             vertices[2],texVertices[2],texNum);
    }

    @Override
    protected float[][][] asTrianglePoints() {
        return points;
    }
    
    @Override
    protected float[][][] triangleTexturePoints(){
        return texturePts;
    }
    
    @Override
    protected int[] textureNumbers(){
        return textureNums;
    }
    
    @Override
    protected void translate(float[] v){
        for(float[] p : points[0]){
            p[0] += v[0];
            p[1] += v[1];
            p[2] += v[2];
        }
    }
    
    @Override
    protected void rotate(Quaternion rotation,float[] temp){
        for(int i = 0; i < 3; i++){
            rotation.rotatePt(this.points[0][i],temp);
            this.points[0][i][0] = temp[0];
            this.points[0][i][1] = temp[1];
            this.points[0][i][2] = temp[2];
        }
    }
    
    @Override
    protected void pointRotate(Quaternion rotation, float[] center, float[] temp){
        for(int i = 0; i < 3; i++){
            this.points[0][i][0] -= center[0];
            this.points[0][i][1] -= center[1];
            this.points[0][i][2] -= center[2];
            rotation.rotatePt(this.points[0][i],temp);
            this.points[0][i][0] = temp[0];
            this.points[0][i][1] = temp[1];
            this.points[0][i][2] = temp[2];
            this.points[0][i][0] += center[0];
            this.points[0][i][1] += center[1];
            this.points[0][i][2] += center[2];
        }
    }
    
    @Override
    protected float[] getNormal(){
        if(this.normal != null) return this.normal;
        this.normal = new float[3];
        float[] w = new float[] {points[0][1][0]-points[0][0][0],
                                  points[0][1][1]-points[0][0][1],
                                  points[0][1][2]-points[0][0][2]};
        float[] v = new float[] {points[0][2][0]-points[0][0][0],
                                  points[0][2][1]-points[0][0][1],
                                  points[0][2][2]-points[0][0][2]};
        normal[0] = v[1]*w[2] - v[2]*w[1];
        normal[1] = v[2]*w[0] - v[0]*w[2];
        normal[2] = v[0]*w[1] - v[1]*w[0];
        FastFMath.normalizeVec(normal);
        return normal;
    }

    @Override
    void swapTexture(int newTexId) {
        this.textureNums[0] = newTexId;
    }
    
    @Override
    public boolean rayTraceIntersect(float[] dir, float[] origin){
        float[] norm = this.getNormal();
        float t = norm[0]*(points[0][0][0]-origin[0]) + 
                  norm[1]*(points[0][0][1]-origin[1]) + 
                  norm[2]*(points[0][0][2]-origin[2]);
        t/= (norm[0]*dir[0] + 
             norm[1]*dir[1] + 
             norm[2]*dir[2]);
        float[] intersect = new float[] {
            origin[0] + dir[0]*t - points[0][0][0],
            origin[1] + dir[1]*t - points[0][0][1],
            origin[2] + dir[2]*t - points[0][0][2]};
        System.out.println("inter: " + Arrays.toString(intersect));
        float[] side = 
            new float[] {
                points[0][1][0] - points[0][0][0],
                points[0][1][1] - points[0][0][1],
                points[0][1][2] - points[0][0][2]
            };
        if(!inFront(side,norm,intersect)) return false;
        side[0] = points[0][2][0] - points[0][1][0];
        side[1] = points[0][2][1] - points[0][1][1];
        side[2] = points[0][2][2] - points[0][1][2];
        if(!inFront(side,norm,intersect)) return false;
        side[0] = points[0][0][0] - points[0][2][0];
        side[1] = points[0][0][1] - points[0][2][1];
        side[2] = points[0][0][2] - points[0][2][2];
        return inFront(side,norm,intersect);
    }
    
    //true if point is in front of axb plane
    private static boolean inFront(float[] a, float[] b,float[] pt){
        return (a[1]*b[2] - b[1]*a[2])*pt[0] + 
               (a[2]*b[0] - b[2]*a[0])*pt[1] + 
               (a[0]*b[1] - b[0]*a[1])*pt[2] >= 0;
    }
}
