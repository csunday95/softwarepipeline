/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.util.Arrays;
import transform.FastFMath;
import transform.Quaternion;

/**
 * //TODO: need to restrict this so always lies in a single plane for BSP
 * 
 * @author Christopher
 */
public class Quad extends Primitive{
    
    private final float[][][] points;
    private final int[] texNums;
    private final float[][][] texPoints;
    private float[] normal;
    
    /**
     * Create a Quad primitive. Points A,B,C,D are specified in counterclockwise
     * order, and are treated as specifying two triangles [A,B,C] and [C,D,A].
     * 
     * @param a point A
     * @param texA texture point A
     * @param b point B
     * @param texB texture point B
     * @param c point C
     * @param texC texture point C
     * @param d point D
     * @param texD texture point D
     * @param texNum texture id
     */
    public Quad(float[] a, float[] texA, float[] b, float[] texB, float[] c, float[] texC,
            float[] d, float[] texD, int texNum){
        //seperate into 2 triangles
        points = new float[][][] {{a,b,c},{c,d,a}};
        texNums = new int[] {texNum,texNum};
        texPoints = new float[][][] {{texA,texB,texC},{texC,texD,texA}};
        this.normal = null;
        //testing code
//        this.normal = this.getNormal();
//        float[] n2 = new float[3];
//        float[] v = new float[] {points[1][1][0]-points[0][2][0],
//                                  points[1][1][1]-points[0][2][1],
//                                  points[1][1][2]-points[0][2][2]};
//        float[] w = new float[] {points[0][0][0]-points[0][2][0],
//                                  points[0][0][1]-points[0][2][1],
//                                  points[0][0][2]-points[0][2][2]};
//        n2[0] = v[1]*w[2] - v[2]*w[1];
//        n2[1] = v[2]*w[0] - v[0]*w[2];
//        n2[2] = v[0]*w[1] - v[1]*w[0];
//        System.out.println("n1: " + Arrays.toString(normal));
//        System.out.println("n2: " + Arrays.toString(n2) + "\n");
    }
    
    @Override
    protected float[][][] asTrianglePoints() {
        return points;
    }

    @Override
    protected float[][][] triangleTexturePoints() {
        return texPoints;
    }

    @Override
    protected int[] textureNumbers() {
        return texNums;
    }
    
    @Override
    protected void swapTexture(int newTexId){
        this.texNums[0] = newTexId;
        this.texNums[1] = newTexId;
    }
    
    @Override
    protected void translate(float[] v){
        for(float[] p : points[0]){
            p[0] += v[0];
            p[1] += v[1];
            p[2] += v[2];
        }
        points[1][1][0] += v[0];
        points[1][1][1] += v[1];
        points[1][1][2] += v[2];
    }
    @Override
    protected void pointRotate(Quaternion rotation, float[] center, float[] temp){
        //adjust a,b,c
        for(int i = 0; i < 3; i++){
            this.points[0][i][0] -= center[0];
            this.points[0][i][1] -= center[1];
            this.points[0][i][2] -= center[2];
            rotation.rotatePt(this.points[0][i],temp);
            this.points[0][i][0] = temp[0];
            this.points[0][i][1] = temp[1];
            this.points[0][i][2] = temp[2];
            this.points[0][i][0] += center[0];
            this.points[0][i][1] += center[1];
            this.points[0][i][2] += center[2];
        }
        //adjust d
        this.points[1][1][0] -= center[0];
        this.points[1][1][1] -= center[1];
        this.points[1][1][2] -= center[2];
        rotation.rotatePt(this.points[1][1],temp);
        this.points[1][1][0] = temp[0];
        this.points[1][1][1] = temp[1];
        this.points[1][1][2] = temp[2];
        this.points[1][1][0] += center[0];
        this.points[1][1][1] += center[1];
        this.points[1][1][2] += center[2];
    }
    
    @Override
    protected void rotate(Quaternion rotation,float[] temp){
        //adjust a,b,c
        for(int i = 0; i < 3; i++){
            rotation.rotatePt(this.points[0][i],temp);
            this.points[0][i][0] = temp[0];
            this.points[0][i][1] = temp[1];
            this.points[0][i][2] = temp[2];
        }
        //adjust d
        rotation.rotatePt(this.points[1][1],temp);
        this.points[1][1][0] = temp[0];
        this.points[1][1][1] = temp[1];
        this.points[1][1][2] = temp[2];
    }
    
    @Override
    protected float[] getNormal(){
        if(this.normal != null) return this.normal;
        this.normal = new float[3];
        float[] w = new float[] {points[0][1][0]-points[0][0][0],
                                  points[0][1][1]-points[0][0][1],
                                  points[0][1][2]-points[0][0][2]};
        float[] v = new float[] {points[0][2][0]-points[0][0][0],
                                  points[0][2][1]-points[0][0][1],
                                  points[0][2][2]-points[0][0][2]};
        normal[0] = v[1]*w[2] - v[2]*w[1];
        normal[1] = v[2]*w[0] - v[0]*w[2];
        normal[2] = v[0]*w[1] - v[1]*w[0];
        FastFMath.normalizeVec(normal);
        System.out.println(Arrays.toString(normal));
        return this.normal;
    }
    
    @Override
    public boolean rayTraceIntersect(float[] dir, float[] origin){
        float[] norm = this.getNormal();
        float t = norm[0]*(points[0][0][0]-origin[0]) + 
                  norm[1]*(points[0][0][1]-origin[1]) + 
                  norm[2]*(points[0][0][2]-origin[2]);
        System.out.println("t: " + t);
        t/= (norm[0]*dir[0] + 
             norm[1]*dir[1] + 
             norm[2]*dir[2]);
        System.out.println("tdiv: " + t);
        float[] intersect = new float[] {
            origin[0] + dir[0]*t,
            origin[1] + dir[1]*t,
            origin[2] + dir[2]*t};
        System.out.println("inter: " + Arrays.toString(intersect));
        float[] side = 
            new float[] {
                points[0][1][0] - points[0][0][0],
                points[0][1][1] - points[0][0][1],
                points[0][1][2] - points[0][0][2]
            };
        float[] tempInter = new float[3];
        tempInter[0] = intersect[0] - points[0][0][0];
        tempInter[1] = intersect[1] - points[0][0][1];
        tempInter[2] = intersect[2] - points[0][0][2];
        System.out.println("1");
        if(!inFront(side,norm,tempInter)){
            System.out.println("temp: " + Arrays.toString(tempInter));
            System.out.println("side: " + Arrays.toString(side));
            return false;
        }
        side[0] = points[0][2][0] - points[0][1][0];
        side[1] = points[0][2][1] - points[0][1][1];
        side[2] = points[0][2][2] - points[0][1][2];
        tempInter[0] = intersect[0] - points[0][1][0];
        tempInter[1] = intersect[1] - points[0][1][1];
        tempInter[2] = intersect[2] - points[0][1][2];
        System.out.println("2");
        if(!inFront(side,norm,tempInter)){
            System.out.println("temp: " + Arrays.toString(tempInter));
            System.out.println("side: " + Arrays.toString(side));
            return false;
        }
        side[0] = points[1][1][0] - points[0][2][0];
        side[1] = points[1][1][1] - points[0][2][1];
        side[2] = points[1][1][2] - points[0][2][2];
        tempInter[0] = intersect[0] - points[0][2][0];
        tempInter[1] = intersect[1] - points[0][2][1];
        tempInter[2] = intersect[2] - points[0][2][2];
        System.out.println("3");
        if(!inFront(side,norm,tempInter)){
            System.out.println("temp: " + Arrays.toString(tempInter));
            System.out.println("side: " + Arrays.toString(side));
            return false;
        }
        side[0] = points[0][0][0] - points[1][1][0];
        side[1] = points[0][0][1] - points[1][1][1];
        side[2] = points[0][0][2] - points[1][1][2];
        tempInter[0] = intersect[0] - points[1][1][0];
        tempInter[1] = intersect[1] - points[1][1][1];
        tempInter[2] = intersect[2] - points[1][1][2];
        System.out.println("4");
        return inFront(side,norm,tempInter);
        
    }
    
    //true if point is in front of axb plane
    private static boolean inFront(float[] a, float[] b,float[] pt){
        return (a[1]*b[2] - b[1]*a[2])*pt[0] + 
               (a[2]*b[0] - b[2]*a[0])*pt[1] + 
               (a[0]*b[1] - b[0]*a[1])*pt[2] >= 0;
    }
}
