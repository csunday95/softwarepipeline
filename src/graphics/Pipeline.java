/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.io.File;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritablePixelFormat;
import javafx.concurrent.Task;
import javafx.concurrent.Service;
import transform.FastFMath;
import transform.Quaternion;
import util.TextureManager;

/**
 * 3D Graphics pipeline class. User registers world space objects with
 * various texture and metadata; pipeline renders each frame according to current
 * configuration and copies the result to a specified image. 
 * 
 * @author Christopher
 */
public class Pipeline extends Service<Integer> {
    //max recursion depth when triangulating 
    private final int MAX_REC_DEPTH;
    
    //max size of screen space bounding box for affine mapping
    private final int MAX_REC_SIZE;
    
    private boolean DEBUG = false;
    
    private boolean halted = false;
    
    //perspective projection utility class
    private final PerspectiveProject pProj;
    
    //screen buffer as an RGBA unsigned byte array
    //len: screenX*screenY*4
    private final byte[] buffer;
    
    //texture manager utility class
    private final TextureManager[] textureManagers;
    
    //temp array for world space coordinates in triangulatePoly
    //len: 9
    private final float[] tri;
    
    private final float[] normal;
    
    //temp array for projected screen space coordinates in triangulatePoly
    //len: 6
    private final float[] projP;
    
    private float[] camPos;
    
    private float[] camDir;
    
    private float[] rightDir;
    
    private float[] upDir;
    
    private float camPosX,camPosY,camPosZ;
    
    //temp array for corresponding u,v coordinates in triangulatePoly
    //len: 6
    private final float[] corresponding;
    
    //temp array for first basis vector for world space of triangle in triPoly
    //this is essentially tri[1]-tri[0]
    //len: 3
    private final float[] v3D1;
    
    //temp array for second basis vector (tri[2] - tri[0]); together with 
    //v3D1 this forms a basis in the plane of the world space triangle
    //len: 3
    private final float[] v3D2;
    
    //temp array for holding the world space midpoint coordinates going from
    //point 0 to point 1;this and the 2 arrays below allow for splitting up
    //a triangle into strictly smaller triangles in world space
    //len: 3
    private final float[] mp01;
    
    //temp array for midpoint world space coordinates of point 0/point 2
    //len: 3
    private final float[] mp02;
    
    //temp array for midpoint world space coordinates of point 1/point 2
    //len: 3
    private final float[] mp12;
    
    //temp array for corresponding texture coordinate midpoint (p0/p1)
    //len: 2
    private final float[] texmp01;
    
    //temp array for corresponding texture coordinate midpoint (p0/p2)
    //len: 2
    private final float[] texmp02;
    
    //temp array for corresponding texture coordinate midpoint (p1/p2)
    //len: 2
    private final float[] texmp12;
    
    //possible configurations of a divided triangle along the multicore render line
    private enum DivideResult {TRIFRONT,QUADFRONT,SINGLEFRONT,SINGLEBEHIND,DOUBLE,INPLANE};
    //temp arrays used for computing triangles divided along vertical plane
    private final float[] divideTri1;
    private final float[] divideTri2;
    private final float[] divideTri3;
    private final float[] divideTri1Proj;
    private final float[] divideTri2Proj;
    private final float[] divideTri3Proj;
    private final float[] divideProjI1;
    private final float[] divideProjI2;
    private final float[] divideTex1;
    private final float[] divideTex2;
    private final float[] divideTex3;
    
    //single precision depth buffer used for depth draw detection; when 
    //drawing a pixel, the result is only pushed to the screen buffer if 
    //the corresponding 3d point is "in front of" whatever point was projected
    //to that screen pixel prior.
    //len: screenX*screenY*4
    private final float[] zBufferUpper;
    private final float[] zBufferLower;
    
    //screen size in the horizontal dimension
    private final int xSize;
    
    //screen size in the vertical dimension
    private final int ySize;
    
    //cached value for half of screen width
    private final int xSizeH;
    
    //cached value for half of screen height
    private final int ySizeH;
    
    //distance in world space the projection plane is from the camera
    private final float viewDist;
    
    //cached viewDist^2
    private final float vDistSq;
    
    //array of screen space coordinate buffers; for each level of recursion 
    //there is a buffer to hold the 4 subdivided triangles to (possibly) be further
    //processed in the next layer of recursion.
    //len: MAX_REC_DEPTH
    private final FloatBuffer[] triangBuffers;
    
    //same as triangBuffers for texture coordinates
    //len: MAX_REC_DEPTH
    private final FloatBuffer[] triTexBuffers;
    
    private final LinkedList<float[]>[] scanlines;
    
    //buffer to hold triangles to be rendered in mapTex(). Triangles are in 
    //[A,B,C] point order where the points progress counterclockwise and each
    //point is represented by 3 floats.
    private FloatBuffer triPtBufferUpper;
    private FloatBuffer triPtBufferLower;
    
    //buffer to the projected points corresponding to the triangle in triPtBuffer.
    //Each screen space point is represented by 2 floats. 
    private FloatBuffer projPtBufferUpper;
    private FloatBuffer projPtBufferLower;
    
    //Buffer for the texture space points corresponding to the triangle at the
    //same position in triPtBuffer. 2 floats per point.
    private FloatBuffer triTxBufferUpper;
    private FloatBuffer triTxBufferLower;
    
    //texture id mapped to the triangle in the corresponding position of triPtBuffer
    private IntBuffer triTxNumBufferUpper;
    private IntBuffer triTxNumBufferLower;
    
    //unit normals buffer
    private FloatBuffer triUnitNormalsUpper;
    private FloatBuffer triUnitNormalsLower;
    
    //rigid body ID of the most recent skybox
    private int skyboxId;
    
    //frame number of the current frame
    private int frameNum = 0;
    
    //for checking framerate
    private long startTime;
    
    //internal use for figuring out optimizations
    private long start;
    private final HashMap<String,Long> timings;
    private final HashMap<String,Long> totals;
    private final StringBuilder feedbackAcc;
    private int pixRendered;
    private int pixConsidered;
    private boolean paused;
    
    //pixel format used in the canvas that the screen buffer is drawn to
    private final WritablePixelFormat format;
    
    //list of primatives rendered each frame
    private final ArrayList<Primitive> dynamicContents;
    
    private int dynamicSize;
    
    //primitives that can be placed into BSP tree for optimization
    private final ArrayList<Primitive> staticContents;
    
    //BSPTree for static primitives
    private BSPTree bspTree;
    
    //temp float array used in computing the rotation of a primative
    //len: 3
    private final float[] primRotateTemp;
    
    //list of subsets of primatives in each rigid body
    private final ArrayList<ArrayList<Primitive>> rigidBodyList;
    
    //state variable indicating if a primative will be added to the current rigid
    //body.
    private boolean accumulatingRigidBody;
    
    //Arraylist of float arrays indicating the position and intensity of 
    //a global light. Each array is [x,y,z,I,M] where I is the max intensity
    //and is a number between 0 and 1; M is the min intensity, also between 0 and 1
    private final ArrayList<float[]> globalPointLights;
    
    //Arraylist of float arrays indicating a normal light as an [x,y,z,max,min] unit vector.
    //Illumination will be based on the dot product of the unit normal and the
    //light normal. max indicates the light intensity, [0,1] while min indicates
    //the floor intensity.
    private final ArrayList<float[]> globalNormalLights;
    
    //state variable indicating if backface culling is on; if the normal vector
    //determined by [B-A]x[C-A] differs from the camera direction by over 180
    //degrees the triangle is not rendered
    private boolean backfaceCull;
    
    //state variable indicating if bilinear texture filtering is to be used; 
    //if true, bilinear filtering is used; if false, nearest neighbor is used
    private boolean bilinearMapping;
    
    //indicates if lighting computation is to be done
    private boolean computeLighting;
    
    //number of triangles in use
    private int triNumber;
    
    //cached value for an FOV of 90 degrees
    private final double FOVCos = Math.cos(Math.PI/4);
    private PixelWriter flipBuffer;
    private byte[] screenBuffer;

    public Pipeline(int xSize, int ySize, float viewDist, float[] initPos,
                    double[] initOrientation,int maxRecDepth, int maxRecSize){
        int screenDim = xSize < ySize ? xSize : ySize;
        pProj = new PerspectiveProject(viewDist, initPos, initOrientation,screenDim);
        //rgba order
        this.buffer = new byte[xSize*ySize*4];
        this.textureManagers = new TextureManager[2];
        this.textureManagers[0] = new TextureManager();
        this.textureManagers[1] = new TextureManager(this.textureManagers[0]);
        
        projP = new float[6];
        tri = new float[9];
        normal = new float[3];
        corresponding = new float[6];
        v3D1 = new float[3];
        v3D2 = new float[3];
        mp01 = new float[3];
        mp02 = new float[3];
        mp12 = new float[3];
        texmp01 = new float[2];
        texmp02 = new float[2];
        texmp12 = new float[2];
        divideTri1 = new float[9];
        divideTri2 = new float[9];
        divideTri3 = new float[9];
        divideProjI1 = new float[2];
        divideProjI2 = new float[2];
        divideTri1Proj = new float[6];
        divideTri2Proj = new float[6];
        divideTri3Proj = new float[6];
        divideTex1 = new float[6];
        divideTex2 = new float[6];
        divideTex3 = new float[6];
        zBufferUpper = new float[xSize*ySize];
        zBufferLower = new float[xSize*ySize];
        scanlines = new LinkedList[ySize];
        for(int i = 0; i < ySize; i++){
            scanlines[i] = new LinkedList<>();
        }
        this.MAX_REC_DEPTH = maxRecDepth;
        this.MAX_REC_SIZE = maxRecSize;
        triangBuffers = new FloatBuffer[MAX_REC_DEPTH+1];
        triTexBuffers = new FloatBuffer[MAX_REC_DEPTH+1];
        //probably overkill bounds
        for(int i = 0; i < MAX_REC_DEPTH+1; i++){
            triangBuffers[i] = FloatBuffer.allocate(9*(int)Math.pow(4, i));
            triTexBuffers[i] = FloatBuffer.allocate(9*(int)Math.pow(4, i));
        }
        this.globalPointLights = new ArrayList<>();
        this.globalNormalLights = new ArrayList<>();
        //zbuffer always filled with max distance of infinity between frames
        Arrays.fill(zBufferUpper, Float.POSITIVE_INFINITY);
        Arrays.fill(zBufferLower, Float.POSITIVE_INFINITY);
        this.xSize = xSize;
        this.ySize = ySize;
        this.xSizeH = xSize / 2;
        this.ySizeH = ySize / 2;
        this.format = PixelFormat.getByteBgraPreInstance();
        this.dynamicContents = new ArrayList<>();
        this.dynamicSize = 0;
        this.staticContents = new ArrayList<>();
        this.rigidBodyList = new ArrayList<>();
        this.primRotateTemp = new float[3];
        this.flipBuffer = null;
        this.accumulatingRigidBody = false;
        this.backfaceCull = false;
        this.bilinearMapping = false;
        this.computeLighting = false;
        this.triNumber = 0;
        this.viewDist = viewDist;
        this.vDistSq = viewDist*viewDist;
        this.startTime = 0;
        this.timings = new HashMap<>();
        this.totals = new HashMap<>();
        for(String s : new String[] {"color clear",
                                     "queues     ",
                                     "zbuff clear", 
                                     "All primit ",
                                     "set pixels ",
                                     "triang poly",
                                     "map tex    "}){
            this.totals.put(s, (long)0);
        }
        this.feedbackAcc = new StringBuilder();
        start = 0;
        this.skyboxId=-1;
        tri[0] = -5;tri[1] = -5;tri[2] = 5;
        tri[3] = 1;tri[4] = -5;tri[5] = 0;
        tri[6] = 3;tri[7] = 10;tri[8] = -5;
        corresponding[0] = 0;corresponding[1] = 0;
        corresponding[2] = 10;corresponding[3] = 0;
        corresponding[4] = 0;corresponding[5] = 10;
        this.camPos = new float[3];
        this.camDir = new float[3];
        this.rightDir = new float[3];
        this.upDir = new float[3];
        this.paused = false;
    }
    
    public Pipeline(int xSize, int ySize, float viewDist, float[] initPos,
                    double[] initOrientation, PixelWriter flipBuffer,
                    int maxRecDepth, int maxRecSize){
        this(xSize,ySize,viewDist,initPos,initOrientation,maxRecDepth,maxRecSize);
        this.flipBuffer = flipBuffer;
    }
    
    public Pipeline(int xSize, int ySize, float viewDist, Path resources, float[] initPos,
                    double[] initOrientation, PixelWriter flipBuffer,
                    int maxRecDepth, int maxRecSize){
        this(xSize,ySize,viewDist,initPos,initOrientation,maxRecDepth,maxRecSize);
        this.flipBuffer = flipBuffer;
        this.textureManagers[0].addTextures(resources.toFile());
    }
    
    public Pipeline(int xSize, int ySize, float viewDist, float[] initPos,
                    double[] initOrientation, byte[] screenBuffer,
                    int maxRecDepth, int maxRecSize){
        this(xSize,ySize,viewDist,initPos,initOrientation,maxRecDepth,maxRecSize);
        this.screenBuffer = screenBuffer;
    }
    
    public class HaltedException extends Exception{
        private final int framesRendered;
        public HaltedException(int framesRendered){
            this.framesRendered = framesRendered;
        }
        
        @Override
        public String toString(){
            return "Pipeline Exited Normally\n\nFrames Rendered: " + 
                    Integer.toString(this.framesRendered) + "\n";
        }
    }
    
    /**
     * Task used to render a single frame on a worker thread.
     */
    private class RenderTask extends Task<Integer>{
        //number of this frame
        private final int frameNum;
        
        /**
         * Creates a frame render Task with the frame number supplied
         * 
         * @param frameNum frame number of this task
         */
        public RenderTask(int frameNum){
            this.frameNum = frameNum;
        }
        
        /**
         * Renders a frame to the pipeline's buffer
         * @return the frame number of this Task
         * @throws Exception passes up exception to service
         */
        @Override
        protected Integer call() throws Exception{
            renderFrame();
            return frameNum;
        }
    }
    
    private class TexMapThread extends Thread{
        
        private final boolean upper;
        public TexMapThread(boolean upper){
            this.upper = upper;
        }
        
        @Override
        public void run(){
            mapTex(upper);
        }
    }
    
    /**
     * Method to be used by javafx Worker framework when start() is called
     * 
     * @return the created task
     */
    @Override
    protected Task createTask(){
        return new RenderTask(frameNum++);
    } 
    
    /**
     * @return the number of the frame currently being rendered
     */
    public int getFrameNumber(){
        return this.frameNum;
    }
    
    /**
     * @return the time the most recent frame started
     */
    public long getStartTime(){
        return this.startTime;
    }
    
    /**
     * @return String representation of the current configuration 
     */
    public String getConfigString(){
        return "Bilin: " + this.bilinearMapping + 
               "\nBackface cull: " + this.backfaceCull + 
               "\nLighting: " + this.computeLighting + 
               "\nViewDir: " + Arrays.toString(this.getViewDir())+
               "\nCamPos: " + Arrays.toString(this.pProj.camLoc());
    }
    
    /**
     * allocates buffers based on the MAX_RECURSION_SIZE
     */
    public void allocateBuffers(){
        int totalSize = 0;
        if(!this.staticContents.isEmpty()){
            this.bspTree = new BSPTree(this.staticContents.remove(this.staticContents.size()-1));
            this.bspTree.build(staticContents);
            totalSize = this.bspTree.size();
        }
        else{
            this.bspTree = new BSPTree();
        }
        totalSize+=this.dynamicSize;
        triPtBufferUpper = FloatBuffer.allocate(totalSize*9*(int)Math.pow(4,MAX_REC_DEPTH));
        projPtBufferUpper = FloatBuffer.allocate(totalSize*6*(int)Math.pow(4,MAX_REC_DEPTH));
        triTxBufferUpper = FloatBuffer.allocate(totalSize*6*(int)Math.pow(4,MAX_REC_DEPTH));
        triUnitNormalsUpper = FloatBuffer.allocate(totalSize*3*(int)Math.pow(4,MAX_REC_DEPTH));
        triTxNumBufferUpper = IntBuffer.allocate(totalSize*(int)Math.pow(4,MAX_REC_DEPTH));
        triPtBufferLower = FloatBuffer.allocate(totalSize*9*(int)Math.pow(4,MAX_REC_DEPTH));
        projPtBufferLower = FloatBuffer.allocate(totalSize*6*(int)Math.pow(4,MAX_REC_DEPTH));
        triTxBufferLower = FloatBuffer.allocate(totalSize*6*(int)Math.pow(4,MAX_REC_DEPTH));
        triTxNumBufferLower = IntBuffer.allocate(totalSize*(int)Math.pow(4,MAX_REC_DEPTH));
        triUnitNormalsLower = FloatBuffer.allocate(totalSize*3*(int)Math.pow(4,MAX_REC_DEPTH));
    }
    
    /**
     * renders a single frame to the screen buffer according to the current 
     * render configuration.
     * @throws graphics.Pipeline.HaltedException raised to indicate that 
     * the pipeline has finished rendering
     */
    public void renderFrame() throws HaltedException{
        if(this.halted){
            throw new HaltedException(this.frameNum);
        }
        this.startTime = System.nanoTime();
        if(this.paused) return;
        this.pProj.recalculateViewDir();
        //start using this to figure out where all the time goes
        if(DEBUG) this.start = System.nanoTime();
        this.triNumber = 0;
        this.pixRendered = 0;
        this.pixConsidered = 0;
        Arrays.fill(this.buffer, (byte)0);
        if(DEBUG) this.timings.put("color clear", System.nanoTime()-this.start);
        if(DEBUG) this.start = System.nanoTime();
        
        if(DEBUG) this.timings.put("queues     ", System.nanoTime()-this.start);
        if(DEBUG) this.start = System.nanoTime();
        Arrays.fill(zBufferUpper, Float.POSITIVE_INFINITY);
        Arrays.fill(zBufferLower, Float.POSITIVE_INFINITY);
        if(DEBUG) this.timings.put("zbuff clear", System.nanoTime()-this.start);
        if(DEBUG) this.start = System.nanoTime();
        float[][][] triPoints;
        float[][][] texPoints;
        int[] textureNumbers;
        long temp;
        this.camPos = this.pProj.camLoc();
        this.camDir = this.pProj.viewDir();
        this.rightDir = this.pProj.rightDir();
        this.upDir = this.pProj.upDir();
        this.camPosX = camPos[0];
        this.camPosY = camPos[1];
        this.camPosZ = camPos[2];
        float[] v = new float[] {-camPos[0],-camPos[1],-camPos[2]};
        if(!(this.skyboxId==-1)) this.translateRigidBody(this.skyboxId, v);
        if(DEBUG) this.timings.put("triang poly",(long)0);
        if(DEBUG) this.timings.put("map tex    ",(long)0);
        this.triPtBufferUpper.clear();
        this.projPtBufferUpper.clear();
        this.triUnitNormalsUpper.clear();
        this.triTxBufferUpper.clear();
        this.triTxNumBufferUpper.clear();
        this.triPtBufferLower.clear();
        this.projPtBufferLower.clear();
        this.triUnitNormalsLower.clear();
        this.triTxBufferLower.clear();
        this.triTxNumBufferLower.clear();
        Iterable<Primitive>[] contents = new Iterable[2];
        contents[0] = this.bspTree.nearestFarthestTraversal(camPos);
        contents[1] = this.dynamicContents;
        for(int lst = 0; lst < 2; lst++){
            for (Primitive pr : contents[lst]) {
                if(!pr.isVisible()) continue;
                triPoints = pr.asTrianglePoints();
                texPoints = pr.triangleTexturePoints();
                textureNumbers = pr.textureNumbers();
                for (int i = 0; i < triPoints.length; i++) {
                    for(int j = 0; j <= MAX_REC_DEPTH; j++) triangBuffers[j].clear();
                    for(int j = 0; j <= MAX_REC_DEPTH; j++) triTexBuffers[j].clear();
                    //put on initial world space tri
                    for(float[] vertex : triPoints[i])triangBuffers[0].put(vertex);
                    for(float[] corrVertex : texPoints[i]) triTexBuffers[0].put(corrVertex);
                    temp = System.nanoTime();
                    this.triangulatePoly(textureNumbers[i]);
                    if(DEBUG) this.timings.put("triang poly",this.timings.get("triang poly") + (System.nanoTime()-temp));
                }
            }
        }
        triPtBufferUpper.limit(triPtBufferUpper.position());
        triPtBufferLower.limit(triPtBufferLower.position());
        temp = System.nanoTime();
        TexMapThread tu = new TexMapThread(true);
        TexMapThread tl = new TexMapThread(false);
        tu.start();
        tl.start();
        try{
            tu.join();
            tl.join();
        }
        catch(InterruptedException ex){
            System.out.println("hwhat");
        }
        if(DEBUG) this.timings.put("map tex    ", System.nanoTime() - temp);
        if(DEBUG) this.timings.put("All primit ", System.nanoTime()-this.start);
        if(DEBUG) this.start = System.nanoTime();
        if(this.flipBuffer != null)this.flipBuffer.setPixels(0, 0, xSize, ySize, format, buffer, 0, xSize*4);
        else System.arraycopy(buffer, 0, this.screenBuffer, 0, buffer.length);
        if(DEBUG) this.timings.put("set pixels ", System.nanoTime()-this.start);
        if(DEBUG) this.start = System.nanoTime();
        if(DEBUG){
            for(Entry<String,Long> e : this.timings.entrySet()){
                this.totals.put(e.getKey(), this.totals.get(e.getKey()) + e.getValue());
                this.feedbackAcc.append(e.getKey()).append(": ").append(e.getValue()).append("\n");
            }
        }
        if(DEBUG)this.feedbackAcc.append("\ntotals:\n");
        for(Entry<String,Long> e : this.totals.entrySet()){
            this.feedbackAcc.append(e.getKey()).append(": ").append(e.getValue()).append("\n");
        }
        this.feedbackAcc.append("\n\n");
        if(DEBUG)System.out.print(this.feedbackAcc.toString());
        if(DEBUG) System.out.println("rendered " + this.triNumber + " triangles\n" + "pix consid: " + this.pixConsidered + "\n" +  
                           "pix rend: " + this.pixRendered);
        //clear
        if(!(this.skyboxId == -1))this.translateRigidBody(this.skyboxId, camPos);
        if(DEBUG)this.feedbackAcc.setLength(0);
    }
    
    /**
     * @param name the filename of the texture desired
     * @return the index of the texture
     */
    public int getTexId(String name){
        return this.textureManagers[0].getTexIndexByName(name);
    }
    
    /**
     * method to get the size dimensions of a given registered texture.
     * @param texId the index of the texture 
     * @return the [x,y] dimension of that texture
     */
    public int[] getTexDimensions(int texId){
        return this.textureManagers[0].getTexDimByIdInt(texId);
    }
    
    /**
     * Add a triangle to the contents of this pipeline. The triangle must be 
     * specified by 3 float arrays of length 3 that correspond to points a,b,c.
     * [a,b,c] must be given in counterclockwise order, and the "normal direction"
     * of the triangle is assumed to be [b-a]x[c-a]. The texture corrdinates 
     * corresponding to a,b,c must also be given in that order as 3 float arrays
     * of length 2. The index of the registered texture must also be given.
     * e.g.
     *                      C
     * 
     * 
     *                                    B
     *                 A 
     * 
     * @param a 3-float array giving point A
     * @param texA 2-float array giving the texture space coordinate corresponding
     *             to point A
     * @param b 3-float array giving point B
     * @param texB 2-float array giving the texture space coordinate corresponding
     *             to point B
     * @param c 3-float array giving point C
     * @param texC 2-float array giving the texture space coordinate corresponding
     *             to point C
     * @param texNum index of the texture to be mapped to this tri
     * @param isStatic if true, this triangle will be rendered with static optimizations
     * @return the newly added Primitive object
     */
    public Primitive addTri(float[] a, float[] texA, float[] b, float[] texB, float[] c,
            float[] texC, int texNum, boolean isStatic) {
        Primitive pr = new Triangle(a, texA, b, texB, c, texC, texNum);
        if(isStatic) this.staticContents.add(pr);
        else{
            this.dynamicContents.add(pr);
            this.dynamicSize++;
            if(this.accumulatingRigidBody){
                this.rigidBodyList.get(this.rigidBodyList.size()-1).add(pr);
            }
        }
        return pr;
    }
    
    
    /**
     * Add a quad to the contents of this pipeline. The quad must be 
     * specified by 4 float arrays of length 3 that correspond to points a,b,c,d.
     * [a,b,c,d] must be given in counterclockwise order.The quad is subdivided into
     * two triangles consisting of [a,b,c],[c,d,a] and the "normal direction"
     * of the triangles are assumed to be [b-a]x[c-a] and [d-c]x[a-c]. 
     * The texture corrdinates corresponding to a,b,c,d must also be given in 
     * that order as 3 float arrays of length 2. 
     * The index of the registered texture must also be given.
     * 
     * @param a 3-float array giving point A
     * @param texA 2-float array giving the texture space coordinate corresponding
     *             to point A
     * @param b 3-float array giving point B
     * @param texB 2-float array giving the texture space coordinate corresponding
     *             to point B
     * @param c 3-float array giving point C
     * @param texC 2-float array giving the texture space coordinate corresponding
     *             to point C
     * @param d 3-float array giving point D
     * @param texD 2-float array giving the texture space coordinate corresponding
     *             to point D
     * @param texNum index of the texture to be mapped to this quad
     * @param isStatic if this is a static primitive (is never transformed), set this true
     * @return the newly created Primitive
     */
    public Primitive addQuad(float[] a, float[] texA, float[] b, float[] texB, float[] c, 
                             float[] texC, float[] d, float[] texD, int texNum, boolean isStatic) {
        Primitive pr = new Quad(a, texA, b, texB, c, texC, d, texD, texNum);
        if(isStatic){
            this.staticContents.add(pr);
        }
        else{
            this.dynamicContents.add(pr);
            this.dynamicSize+=2;
            if(this.accumulatingRigidBody){
                this.rigidBodyList.get(this.rigidBodyList.size()-1).add(pr);
            }
        }
        return pr;
    }
    
    /**
     * Define a quad as a the parallelogram given by the origin point and 
     * v1,v2. 
     * 
     * @param origin
     * @param v1
     * @param v2
     * @param texnum
     * @param isStatic
     * @return 
     */
    
    public Primitive addQuad(float[] origin, float[] v1, float[] v2,int texnum,boolean isStatic){
        float[] b = new float[] {origin[0] + v1[0], origin[1] + v1[1],origin[2] + v1[2]};
        float[] c = new float[] {origin[0] + v1[0] + v2[0], 
                                 origin[1] + v1[1] + v2[1],
                                 origin[2] + v1[2] + v2[2]};
        float[] d = new float[] {origin[0] + v2[0], origin[1] + v2[1],origin[2] + v2[2]};
        int[] texDims = this.getTexDimensions(texnum);
        float[] texA = new float[] {0,0};
        float[] texB = new float[] {texDims[0],0};
        float[] texC = new float[] {texDims[0],texDims[1]};
        float[] texD = new float[] {0,texDims[1]};
        return this.addQuad(origin, texA, b, texB, c, texC, d, texD, texnum, isStatic);
    }
    
    /**
     * Indicate the start of a rigid body. Primatives added to the pipeline between
     * this call and the next call to endRigidBody() will be registered as a rigid
     * body under the id returned. Rigid bodies may be rotated and translated 
     * as a whole more efficiently than doing so for many Primatives.
     * @return id of the rigid body newly registered
     */
    public int registerRigidBody(){
        this.rigidBodyList.add(new ArrayList<>());
        this.accumulatingRigidBody = true;
        return this.rigidBodyList.size()-1;
    }
    
    /**
     * @return the number of Primatives in the rigid body
     */
    public int endRigidBody(){
        this.accumulatingRigidBody = false;
        return this.rigidBodyList.get(this.rigidBodyList.size()-1).size();
    }
    
    /**
     * toggles backface culling to the opposite of what it was before the call
     */
    public void enableBackfaceCulling(){
        this.backfaceCull = !this.backfaceCull;
    }
    
    /**
     * @param setTo backface culling will be enabled if true; disabled otherwise
     */
    public void enableBackfaceCulling(boolean setTo){
        this.backfaceCull = setTo;
    }
    
    /**
     * toggle bilinear texture filtering to the oppposite of what it was before
     * the call
     */
    public void enableBilinearMapping(){
        this.bilinearMapping = !this.bilinearMapping;
    }
    
    /**
     * @param setTo bilinear mapping will be used on the next frame and henceforth
     *              if true; nearest neighbor mapping will be used otherwise
     */
    public void enableBilinearMapping(boolean setTo){
        this.bilinearMapping = setTo;
    }
    
    public void enableLighting(){
        this.computeLighting = !this.computeLighting;
    }
    
    public void enableLighting(boolean setTo){
        this.computeLighting = setTo;
    }
    
    public void pause(){
        this.paused = !this.paused;
    }
    
    public void enableDebug(){
        this.DEBUG = !this.DEBUG;
    }
    
    public void exit(){
        this.halted = true;
    }
    
    public void enableDebug(boolean setTo){
        this.DEBUG = setTo;
    }
    
    public boolean inDebugMode(){
        return this.DEBUG;
    }
    
    public boolean isPaused(){
        return this.paused;
    }
    
    /**
     * Translate a primative p by the world space vector v. v will be added to every
     * world space vetex in p. 
     * 
     * @param p Primitive to translate
     * @param v 3-float array specifying the amount of world space to translate by
     */
    public void translatePrimative(Primitive p, float[] v){
        p.translate(v);
    }
    
    /**
     * Rotate the single specified primative using the Quaternion provided
     * about the point center.
     * 
     * @param pr Primitive to rotate
     * @param center 3-float array indicating the center in world space to 
     *               rotate about
     * @param q Quaternion to rotate with
     */
    public void rotatePrimative(Primitive pr, float[] center, Quaternion q){
        pr.pointRotate(q, center, this.primRotateTemp);
    }
    
    /**
     * Set the given primitive to render with the new texture as indicated 
     * next frame.
     * 
     * @param pr primitive to set
     * @param newTexId id of new texture 
     */
    public void swapPrimitiveTexture(Primitive pr,int newTexId){
        pr.swapTexture(newTexId);
    }
    
    /**
     * Translate all members of a rigid body at once.
     * 
     * @param rigidBody index of the rigid body to translate
     * @param v 3-float array indicating the world space movement in [x,y,z] order
     */
    public void translateRigidBody(int rigidBody, float[] v){
        for(Primitive pr : this.rigidBodyList.get(rigidBody)){
            pr.translate(v);
        }
    }
    
    /**
     * Rotate all members of a registered rigid body about center using 
     * Quaternion q.
     * 
     * @param rigidBody index of the body to rotate all members of
     * @param center 3-float array indicating the world space point to rotate about
     * @param q Quaternion to use in the rotation
     */
    public void rotateRigidBody(int rigidBody, float[] center, Quaternion q){
        if(this.paused) return;
        for(Primitive pr : this.rigidBodyList.get(rigidBody)){
            pr.pointRotate(q, center, this.primRotateTemp);
        }
    }
    
    /**
     * Register a new texture to use.
     * @param tex File object specifying the location of the .png, .bmp, or .jpg
     * texture to use. These should only be loaded before rendering begins.
     * @return index of the newly registered texture
     */
    public int addTex(File tex) {
        if(this.flipBuffer == null) return this.textureManagers[0].addTextureBufferedImage(tex);
        return this.textureManagers[0].addTexture(tex);
    }
    
    /**
     * Register a skybox {@code dist} away from the camera location using the 
     * texture identified by {@code texNum} for all 6 faces. 
     * 
     * @param dist distance of skybox from camera location
     * @param texNum skybox texture id to use; needs to be same configuration as 
     * template
     */
    public void registerSkybox(float dist, int texNum){
        float[] bot1,bot2,bot3,bot4,top1,top2,top3,top4;
        float[][] texpts = new float[16][2];
        this.skyboxId = this.registerRigidBody();
        //world space coords relative to camera
        bot1 = new float[] {-dist,-dist,-dist};
        bot2 = new float[] {dist,-dist,-dist};
        bot3 = new float[] {dist,dist,-dist};
        bot4 = new float[] {-dist,dist,-dist};
        top1 = new float[] {-dist,-dist,dist};
        top2 = new float[] {dist,-dist,dist};
        top3 = new float[] {dist,dist,dist};
        top4 = new float[] {-dist,dist,dist};
        int[] texDims = this.textureManagers[0].getTexDimByIdInt(texNum);
        float size = texDims[1]/3;
        texpts[0][0] = 0;
        texpts[0][1] = size;
        texpts[1][0] = size;
        texpts[1][1] = size;
        texpts[2][0] = size;
        texpts[2][1] = 2*size;
        texpts[3][0] = 0;
        texpts[3][1] = 2*size;
        texpts[4][0] = 2*size;
        texpts[4][1] = size;
        texpts[5][0] = 2*size;
        texpts[5][1] = 2*size;
        texpts[6][0] = 3*size;
        texpts[6][1] = size;
        texpts[7][0] = 3*size;
        texpts[7][1] = 2*size;
        texpts[8][0] = 4*size;
        texpts[8][1] = size;
        texpts[9][0] = 4*size;
        texpts[9][1] = 2*size;
        texpts[10][0] = 3*size;
        texpts[10][1] = 3*size;
        texpts[11][0] = 2*size;
        texpts[11][1] = 3*size;
        texpts[12][0] = 2*size;
        texpts[12][1] = 0;
        texpts[13][0] = 3*size;
        texpts[13][1] = 0;
        this.addQuad(bot1, texpts[4], bot4, texpts[6], bot3, 
                     texpts[7], bot2, texpts[5], texNum, false);
        this.addQuad(bot2, texpts[6], bot3, texpts[7], top3, 
                     texpts[9], top2, texpts[8], texNum, false);
        this.addQuad(bot1, texpts[4], bot2, texpts[6], top2, 
                     texpts[13], top1, texpts[12], texNum, false);
        this.addQuad(bot3, texpts[5], bot4, texpts[7], top4, 
                     texpts[10], top3, texpts[11], texNum, false);
        this.addQuad(bot4, texpts[4], bot1, texpts[5], top1, 
                     texpts[2], top4, texpts[1], texNum, false);
        this.addQuad(top1, texpts[1], top2, texpts[2], top3, 
                     texpts[3], top4, texpts[0], texNum, false);
        this.endRigidBody();
    }
    
    /**
     * Add a global light to the render pipeline. Texture brightness will be
     * multiplied by the dot product between the surface normal and the light
     * direction times the max, bottoming out at min. 
     * 
     * @param x space X coordinate of the light
     * @param y space Y coordinate of the light
     * @param z space Z coordinate of the light
     * @param max maximum intensity multiplier, number between 0 and 1
     * @param min minimum intensity multiplier regardless of normal direction,
     *        also should be between 0 and 1 and less than max
     * @return ID number of this light
     */
    public int registerPointLight(float x, float y, float z, float max, float min){
        this.globalPointLights.add(new float[] {x,y,z,max,min});
        return this.globalPointLights.size()-1;
    }
    
    public int registerNormalLight(float x, float y, float z, float max, float min){
        this.globalNormalLights.add(new float[] {x,y,z,max,min});
        return this.globalNormalLights.size()-1;
    }
    
    /**
     * Call with a PixelWriter to copy the state of the screen buffer to the 
     * writer's underlying image. Should be used after renderFrame() returns. 
     * 
     * @param writer PixelWriter to copy the screen buffers contents to
     */
    public void writeToFrame(PixelWriter writer) {
        writer.setPixels(0, 0, xSize, ySize, format, buffer, 0, xSize*4);
    }
    
    /**
     * Moves the camera in world space relative to its current position.
     * @param vector 3-float array indicating the amount to move the camera by
     *               in [x,y,z] order.
     */
    public void moveCameraBy(float[] vector) {
        this.pProj.moveCamBy(vector);
    }
    
    public void moveCameraTo(float x, float y, float z){
        this.pProj.setCamPos(x,y,z);
    }
    
    public void moveCameraTo(float[] to){
        this.pProj.setCamPos(to);
    }

    /**
     * Rotate the camera by the angles specified in radians. 
     * @param by 3-double array indicating the amount to rotate the camera by
     *           based on the current orientation in the order [yaw,pitch,roll]
     */
    public void rotateCameraBy(double[] by) {
        this.pProj.queueRot(by);
    }
    
    public void setCamAngle(double[] to) {
        this.pProj.setCameraAngle(to);
    }
    
    public void orientCam(double[] by) {
        this.pProj.rotateCameraBy(by);
    }
    
    /**
     * Returns the world space direction corresponding to the given screen
     * coordinate. 
     * 
     * @param x Screen x coordinate [-maxX/2, maxX/2]
     * @param y Screen y coordinate [-maxY/2, maxY/2]
     * @return 
     */
    public float[] getWorldDir(double x, double y){
        float[] rot = new float[] {
            (float)(x/this.xSizeH),
            (float)(y/this.ySizeH),1};
        float[] ret = new float[3];
        FastFMath.normalizeVec(rot);
        this.pProj.inverseRotatePoint(rot, ret);
        return ret;
    }
    
    //testing function
    public float[][] projPt(float[] p1, float[] p2){
        float[][] out = new float[2][2];
        this.pProj.pointProjectSingle(p1[0], p1[1], p1[2], out[0], 0);
        this.pProj.pointProjectSingle(p2[0], p2[1], p2[2], out[1], 0);
        return out;
    }
    
    /**
     * @return view direction unit vector as a float array
     */
    protected float[] getViewDir() {
        return this.pProj.viewDir;
    }
    
    ///////right and up directions seem to be bugged; need to do a cross product?
    /**
     * @return direction to the "right" of the view direction as a float array.
     */
    protected float[] getRightDir() {
        return this.pProj.rightDir;
    }
    
    /**
     * @return direction "up" from the camera perspective as a float array
     */
    protected float[] getUpDir() {
        return this.pProj.upDir;
    }
    
    public void copyViewDirections(float[] view, float[] right, float[] up){
        view[0] = this.camDir[0];
        view[1] = this.camDir[1];
        view[2] = this.camDir[2];
        right[0] = this.rightDir[0];
        right[1] = this.rightDir[1];
        right[2] = this.rightDir[2];
        up[0] = this.upDir[0];
        up[1] = this.upDir[1];
        up[2] = this.upDir[2];
    }
    
    public void copyCamLoc(float[] camLoc) {
        camLoc[0] = this.camPos[0];
        camLoc[1] = this.camPos[1];
        camLoc[2] = this.camPos[2];
    }
    
    /**
     * @return number of primatives current registered
     */
    public int polyNumber(){
        return this.dynamicContents.size();
    }
    
    /**
     * Subdivides the triangle pushed onto triangBuffers[0] recursively by 
     * creating four smaller triangles using the original triangle vertices and 
     * the midpoints of the edges of the original triangle. The pushed triangle
     * will be recursively subdivided until either the sub triangle has screen space area
     * less than MAX_REC_SIZE or up to MAX_REC_DEPTH number of subdivisions. 
     * @param textureNum texture this triangle uses 
     */
    public void triangulatePoly(int textureNum) {
        FloatBuffer triBuffFrom;
        FloatBuffer texBuffFrom;
        FloatBuffer triBuffTo;
        FloatBuffer texBuffTo;
        
        for(int recurseDepth = 0; recurseDepth < MAX_REC_DEPTH; recurseDepth++){
            triBuffFrom = this.triangBuffers[recurseDepth];
            //move back to beginning of buffers and grab references from arrays
            triBuffFrom.rewind();
            texBuffFrom = this.triTexBuffers[recurseDepth];
            texBuffFrom.rewind();
            triBuffTo = this.triangBuffers[recurseDepth+1];
            texBuffTo = this.triTexBuffers[recurseDepth+1];
            
            //continue subdividing until no triangles are left on any recursion level
            //vertex buffer
            while(triBuffFrom.position() < triBuffFrom.limit()){
                //pop triangle world space and texture space coords off of buffer
                triBuffFrom.get(tri);
                if(recurseDepth == 0){
                    FastFMath.computeNormal(tri, this.normal);
                }
                if(this.normal[0] == 0 && this.normal[1] == 0 && this.normal[2] == 0){
                    continue;
                }
                texBuffFrom.get(corresponding);
                //cull triangles with points behind the camera
                if((tri[0]-camPos[0])*camDir[0]+(tri[2]-camPos[1])*camDir[1]+
                        (tri[2]-camPos[2])*camDir[2] < this.viewDist &&
                   (tri[3]-camPos[0])*camDir[0]+(tri[4]-camPos[1])*camDir[1]+
                        (tri[5]-camPos[2])*camDir[2] < this.viewDist &&
                   (tri[6]-camPos[0])*camDir[0]+(tri[7]-camPos[1])*camDir[1]+
                        (tri[8]-camPos[2])*camDir[2] < this.viewDist){
                    continue;
                }
                if (this.angleClip(tri, camDir, camPos)) continue;
                //compute screen space coords
                this.pProj.pointProject3(tri, projP);
                //reject triangles w/ very tiny contributing area
                if(this.screenArea(.01f)) continue;
                //compute screen space bounding box
                float xmin = Math.min(Math.min(projP[0], projP[2]), projP[4]);
                float xmax = Math.max(Math.max(projP[0], projP[2]), projP[4]);
                float ymin = Math.min(Math.min(projP[1], projP[3]), projP[5]);
                float ymax = Math.max(Math.max(projP[1], projP[3]), projP[5]);
                //if bounding box entirely out of screen, discard this triangle
                if(xmax < -this.xSizeH || xmin > this.xSizeH || ymax < -this.ySizeH || ymin > this.ySizeH) continue;
                //compute and store 3d basis vectors
                v3D1[0] = tri[3] - tri[0];
                v3D1[1] = tri[4] - tri[1];
                v3D1[2] = tri[5] - tri[2];
                v3D2[0] = tri[6] - tri[0];
                v3D2[1] = tri[7] - tri[1];
                v3D2[2] = tri[8] - tri[2];
                //backface culling; normals can be precomputed 
                if (this.backfaceCull &&
                      (v3D2[2] * v3D1[1] - v3D2[1] * v3D1[2]) * (tri[0] - camPos[0])
                    + (v3D1[2] * v3D2[0] - v3D1[0] * v3D2[2]) * (tri[1] - camPos[1])
                    + (v3D1[0] * v3D2[1] - v3D1[1] * v3D2[0]) * (tri[2] - camPos[2]) < 0) {
                    continue;
                }
                //subdivide if triangle too large and not recursed too deep
                if (Math.abs((xmax - xmin) * (ymax - ymin)) > MAX_REC_SIZE &&
                        recurseDepth < MAX_REC_DEPTH - 1) {
                    //comput world space and texture midpoints
                    mp01[0] = (tri[0] + tri[3]) * .5f;
                    mp01[1] = (tri[1] + tri[4]) * .5f;
                    mp01[2] = (tri[2] + tri[5]) * .5f;
                    mp02[0] = (tri[0] + tri[6]) * .5f;
                    mp02[1] = (tri[1] + tri[7]) * .5f;
                    mp02[2] = (tri[2] + tri[8]) * .5f;
                    mp12[0] = (tri[3] + tri[6]) * .5f;
                    mp12[1] = (tri[4] + tri[7]) * .5f;
                    mp12[2] = (tri[5] + tri[8]) * .5f;
                    texmp01[0] = (corresponding[0] + corresponding[2]) *.5f;
                    texmp01[1] = (corresponding[1] + corresponding[3]) *.5f;
                    texmp02[0] = (corresponding[0] + corresponding[4]) *.5f;
                    texmp02[1] = (corresponding[1] + corresponding[5]) *.5f;
                    texmp12[0] = (corresponding[2] + corresponding[4]) *.5f;
                    texmp12[1] = (corresponding[3] + corresponding[5]) *.5f;
                    //push four sub triangles onto world and texture space buffers
                    //p1 -- start of T1
                    triBuffTo.put(tri[0]);triBuffTo.put(tri[1]);triBuffTo.put(tri[2]);
                    texBuffTo.put(corresponding[0]);texBuffTo.put(corresponding[1]);
                    //mp01
                    triBuffTo.put(mp01[0]);triBuffTo.put(mp01[1]);triBuffTo.put(mp01[2]);
                    texBuffTo.put(texmp01[0]);texBuffTo.put(texmp01[1]);
                    //mp02
                    triBuffTo.put(mp02[0]);triBuffTo.put(mp02[1]);triBuffTo.put(mp02[2]);
                    texBuffTo.put(texmp02[0]);texBuffTo.put(texmp02[1]);
                    //mp01 -- start of T2
                    triBuffTo.put(mp01[0]);triBuffTo.put(mp01[1]);triBuffTo.put(mp01[2]);
                    texBuffTo.put(texmp01[0]);texBuffTo.put(texmp01[1]);
                    //p2
                    triBuffTo.put(tri[3]);triBuffTo.put(tri[4]);triBuffTo.put(tri[5]);
                    texBuffTo.put(corresponding[2]);texBuffTo.put(corresponding[3]);
                    //mp12
                    triBuffTo.put(mp12[0]);triBuffTo.put(mp12[1]);triBuffTo.put(mp12[2]);
                    texBuffTo.put(texmp12[0]);texBuffTo.put(texmp12[1]);
                    //mp02 -- start of T3
                    triBuffTo.put(mp02[0]);triBuffTo.put(mp02[1]);triBuffTo.put(mp02[2]);
                    texBuffTo.put(texmp02[0]);texBuffTo.put(texmp02[1]);
                    //mp01
                    triBuffTo.put(mp01[0]);triBuffTo.put(mp01[1]);triBuffTo.put(mp01[2]);
                    texBuffTo.put(texmp01[0]);texBuffTo.put(texmp01[1]);
                    //mp12
                    triBuffTo.put(mp12[0]);triBuffTo.put(mp12[1]);triBuffTo.put(mp12[2]);
                    texBuffTo.put(texmp12[0]);texBuffTo.put(texmp12[1]);
                    //mp02 -- start of T4
                    triBuffTo.put(mp02[0]);triBuffTo.put(mp02[1]);triBuffTo.put(mp02[2]);
                    texBuffTo.put(texmp02[0]);texBuffTo.put(texmp02[1]);
                    //mp12
                    triBuffTo.put(mp12[0]);triBuffTo.put(mp12[1]);triBuffTo.put(mp12[2]);
                    texBuffTo.put(texmp12[0]);texBuffTo.put(texmp12[1]);
                    //p3
                    triBuffTo.put(tri[6]);triBuffTo.put(tri[7]);triBuffTo.put(tri[8]);
                    texBuffTo.put(corresponding[4]);texBuffTo.put(corresponding[5]);
                } else {
                    //no more subidivison, push final results onto buffers mapTex()
                    //will use
                    if(xmin <= 0 && xmax >= 0){
                        //true if divided tri on pos side of rightDir
                        switch(this.subdivideTri(0)){
                            case SINGLEFRONT:
                                System.out.println("front");
                                this.triNumber++;
                                triPtBufferUpper.put(tri);
                                projPtBufferUpper.put(projP);
                                triUnitNormalsUpper.put(normal);
                                triTxBufferUpper.put(corresponding);
                                triTxNumBufferUpper.put(textureNum);
                                break;
                            case SINGLEBEHIND:
                                System.out.println("behind");
                                this.triNumber++;
                                triPtBufferLower.put(tri);
                                projPtBufferLower.put(projP);
                                triUnitNormalsLower.put(normal);
                                triTxBufferLower.put(corresponding);
                                triTxNumBufferLower.put(textureNum);
                                break;
                            case TRIFRONT:
                                this.triNumber+=3;
                                triPtBufferUpper.put(divideTri1);
                                projPtBufferUpper.put(divideTri1Proj);
                                triUnitNormalsUpper.put(normal);
                                triTxBufferUpper.put(divideTex1);
                                triTxNumBufferUpper.put(textureNum);
                                triPtBufferLower.put(divideTri2);
                                projPtBufferLower.put(divideTri2Proj);
                                triUnitNormalsLower.put(normal);
                                triTxBufferLower.put(divideTex2);
                                triTxNumBufferLower.put(textureNum);
                                triPtBufferLower.put(divideTri3);
                                projPtBufferLower.put(divideTri3Proj);
                                triUnitNormalsLower.put(normal);
                                triTxBufferLower.put(divideTex3);
                                triTxNumBufferLower.put(textureNum);
                                break;
                            case QUADFRONT:
                                this.triNumber+=3;
                                triPtBufferLower.put(divideTri1);
                                projPtBufferLower.put(divideTri1Proj);
                                triUnitNormalsLower.put(normal);
                                triTxBufferLower.put(divideTex1);
                                triTxNumBufferLower.put(textureNum);
                                triPtBufferUpper.put(divideTri2);
                                projPtBufferUpper.put(divideTri2Proj);
                                triUnitNormalsUpper.put(normal);
                                triTxBufferUpper.put(divideTex2);
                                triTxNumBufferUpper.put(textureNum);
                                triPtBufferUpper.put(divideTri3);
                                projPtBufferUpper.put(divideTri3Proj);
                                triUnitNormalsUpper.put(normal);
                                triTxBufferUpper.put(divideTex3);
                                triTxNumBufferUpper.put(textureNum);
                                break;
                            case DOUBLE:
                                //tri1 always in front
                                this.triNumber+=2;
                                triPtBufferUpper.put(divideTri1);
                                this.pProj.pointProject3(divideTri1, projP);
                                projPtBufferUpper.put(projP);
                                triUnitNormalsUpper.put(normal);
                                triTxBufferUpper.put(divideTex1);
                                triTxNumBufferUpper.put(textureNum);
                                triPtBufferLower.put(divideTri2);
                                this.pProj.pointProject3(divideTri2, projP);
                                triUnitNormalsLower.put(normal);
                                projPtBufferLower.put(projP);
                                triTxBufferLower.put(divideTex2);
                                triTxNumBufferLower.put(textureNum);
                                break;
                            default:
                                System.out.println("INPLANE");
                                break;
                        }
                    }
                    //lower on left side
                    else if(xmin >= 0){
                        this.triNumber++;
                        triPtBufferUpper.put(tri);
                        projPtBufferUpper.put(projP);
                        triUnitNormalsUpper.put(normal);
                        triTxBufferUpper.put(corresponding);
                        triTxNumBufferUpper.put(textureNum);
                    }
                    else{
                        this.triNumber++;
                        triPtBufferLower.put(tri);
                        projPtBufferLower.put(projP);
                        triUnitNormalsLower.put(normal);
                        triTxBufferLower.put(corresponding);
                        triTxNumBufferLower.put(textureNum);
                    }
                }
            }
            //as they are probably not full as rec goes deeper, set limit at end
            //of use region of the buffers
            triBuffTo.limit(triBuffTo.position());
            texBuffTo.limit(texBuffTo.position());
        }
    }
    
    //returns true if triangle has area less than tolerance
    private boolean screenArea(float tolerance){
        return Math.abs(projP[0]*(projP[3] - projP[5]) + projP[1]*(projP[4] - projP[2]) + 
               projP[2]*projP[5] - projP[3]*projP[4]) < tolerance;
    }
    

    /**
     * subdivides the triangle currently stored in tri and corresponding into
     * 3 triangles, 1 one one side of the vertical plane, 2 on the other, stored
     * into divideTri and divideTex
     * 
     * @param tolerance should probably remove this?
     * @return DivideResult value corresponding to the result of the subdivision
     */
    private DivideResult subdivideTri(float tolerance){
        int minX, minY, minZ,idx1X,idx1Y,idx1Z,idx2X,idx2Y,idx2Z;
        int minTexX,minTexY,tex1X,tex1Y,tex2X,tex2Y;
        float vx,vy,vz,t,tdiv,inter1X,inter1Y,inter1Z,inter2X,inter2Y,inter2Z;
        float texInter1X,texInter1Y,texInter2X,texInter2Y;
        float firstFront,secondFront,thirdFront;
        DivideResult ret;
        firstFront = (tri[0]-camPos[0])*rightDir[0] +
                     (tri[1]-camPos[1])*rightDir[1] +
                     (tri[2]-camPos[2])*rightDir[2];
        secondFront = (tri[3]-camPos[0])*rightDir[0] +
                      (tri[4]-camPos[1])*rightDir[1] +
                      (tri[5]-camPos[2])*rightDir[2];
        thirdFront = (tri[6]-camPos[0])*rightDir[0] +
                     (tri[7]-camPos[1])*rightDir[1] +
                     (tri[8]-camPos[2])*rightDir[2];
        //figure out some better scheme of distinguishing all the difference cases?
        if(Math.abs(firstFront) <= tolerance && 
           Math.abs(secondFront) <= tolerance && 
           Math.abs(thirdFront) <= tolerance){
            //nothing gets pushed
            return DivideResult.INPLANE;
        }
        //idx2 is point on plane
        else if(Math.abs(firstFront) < tolerance){
            idx2X = 0;idx2Y = 1;idx2Z = 2;
            tex2X = 0; tex2Y = 1;
            if(Math.abs(secondFront) <= tolerance){
                //don't need to push anything 
                if(thirdFront < -tolerance) ret = DivideResult.SINGLEBEHIND;
                else ret = DivideResult.SINGLEFRONT;
                return ret;
            }
            else if(Math.abs(thirdFront) <= tolerance){
                //don't need to push anything 
                if(secondFront > tolerance) ret = DivideResult.SINGLEFRONT;
                else ret = DivideResult.SINGLEBEHIND;
                return ret;
            }
            else if(thirdFront > tolerance && secondFront > tolerance){
                return DivideResult.SINGLEFRONT;
            }
            else if(thirdFront < -tolerance && secondFront < -tolerance){
                return DivideResult.SINGLEBEHIND;
            }
            else{
                //front followed by back in divideTri1/divideTri2
                ret = DivideResult.DOUBLE;
                //min is frontal point in case of double
                if(secondFront > tolerance){
                    minX = 3;minY = 4;minZ = 5;
                    idx1X = 6;idx1Y = 7;idx1Z = 8;
                    minTexX = 2;minTexY = 3;
                    tex1X = 4;tex1Y = 5;
                }
                //thirdFront must be > 0
                else{
                    minX = 6;minY = 7;minZ = 8;
                    idx1X = 3;idx1Y = 4;idx1Z = 5;
                    minTexX = 4;minTexY = 5;
                    tex1X = 2;tex1Y = 3;
                }
            }
        }
        else if(Math.abs(secondFront) <= tolerance){
            idx2X = 3;idx2Y = 4;idx2Z = 5;
            tex2X = 2; tex2Y = 3;
            if(Math.abs(firstFront) <= tolerance){
                //don't need to push anything 
                if(thirdFront <= -tolerance) ret = DivideResult.SINGLEBEHIND;
                else ret = DivideResult.SINGLEFRONT;
                return ret;
            }
            else if(Math.abs(thirdFront) <= tolerance){
                //don't need to push anything 
                if(firstFront > tolerance) ret = DivideResult.SINGLEFRONT;
                else ret = DivideResult.SINGLEBEHIND;
                return ret;
            }
            else if(thirdFront > tolerance && firstFront > tolerance){
                return DivideResult.SINGLEFRONT;
            }
            else if(thirdFront < -tolerance && firstFront < -tolerance){
                return DivideResult.SINGLEBEHIND;
            }
            else{
                //front followed by back in divideTri1/divideTri2
                ret = DivideResult.DOUBLE;
                //min is frontal point in case of double
                if(firstFront > tolerance){
                    minX = 0;minY = 1;minZ = 2;
                    idx1X = 6;idx1Y = 7;idx1Z = 8;
                    minTexX = 0;minTexY = 1;
                    tex1X = 4;tex1Y = 5;
                }
                //thirdFront must be > 0
                else{
                    minX = 6;minY = 7;minZ = 8;
                    idx1X = 0;idx1Y = 1;idx1Z = 2;
                    minTexX = 4;minTexY = 5;
                    tex1X = 0;tex1Y = 1;
                }
            }
        }
        else if(Math.abs(thirdFront) <= tolerance){
            idx2X = 6;idx2Y = 7;idx2Z = 8;
            tex2X = 4; tex2Y = 5;
            if(Math.abs(firstFront) <= tolerance){
                //don't need to push anything 
                if(secondFront < -tolerance) ret = DivideResult.SINGLEBEHIND;
                else ret = DivideResult.SINGLEFRONT;
                return ret;
            }
            else if(Math.abs(secondFront) <= tolerance){
                //don't need to push anything 
                if(firstFront > tolerance) ret = DivideResult.SINGLEFRONT;
                else ret = DivideResult.SINGLEBEHIND;
                return ret;
            }
            else if(firstFront > tolerance && secondFront > tolerance){
                return DivideResult.SINGLEFRONT;
            }
            else if(firstFront < -tolerance && secondFront < -tolerance){
                return DivideResult.SINGLEBEHIND;
            }
            else{
                //front followed by back in divideTri1/divideTri2
                ret = DivideResult.DOUBLE;
                //min is frontal point in case of double
                if(firstFront > tolerance){
                    minX = 0;minY = 1;minZ = 2;
                    idx1X = 3;idx1Y = 4;idx1Z = 5;
                    minTexX = 0;minTexY = 1;
                    tex1X = 2;tex1Y = 3;
                }
                //thirdFront must be > 0
                else{
                    minX = 3;minY = 4;minZ = 5;
                    idx1X = 0;idx1Y = 1;idx1Z = 2;
                    minTexX = 2;minTexY = 3;
                    tex1X = 0;tex1Y = 1;
                }
            }
        }
        //none of them are zero
        else if(firstFront > tolerance){
            if(secondFront > tolerance){
                //min is pt 2, is behind
                minX = 6;minY = 7;minZ = 8;
                idx1X = 0;idx1Y = 1;idx1Z = 2;
                idx2X = 3;idx2Y = 4;idx2Z = 5;
                minTexX = 4;minTexY = 5;
                tex1X = 0;tex1Y = 1;
                tex2X = 2;tex2Y = 3;
                ret = DivideResult.QUADFRONT;
            }
            else if(thirdFront < -tolerance){
                //min is pt 0, is in front
                minX = 0;minY = 1;minZ = 2;
                idx1X = 3;idx1Y = 4;idx1Z = 5;
                idx2X = 6;idx2Y = 7;idx2Z = 8;
                minTexX = 0;minTexY = 1;
                tex1X = 2;tex1Y = 3;
                tex2X = 4;tex2Y = 5;
                ret = DivideResult.TRIFRONT;
            }
            else{
                //min is point 1, is behind
                minX = 3;minY = 4;minZ = 5;
                idx1X = 6;idx1Y = 7;idx1Z = 8;
                idx2X = 0;idx2Y = 1;idx2Z = 2;
                minTexX = 2;minTexY = 3;
                tex1X = 4;tex1Y = 5;
                tex2X = 0;tex2Y = 1;
                ret = DivideResult.QUADFRONT;
            }
        }
        else if(thirdFront > tolerance){
            if(secondFront < -tolerance){
                //min is point 2, is in front
                minX = 6;minY = 7;minZ = 8;
                idx1X = 0;idx1Y = 1;idx1Z = 2;
                idx2X = 3;idx2Y = 4;idx2Z = 5;
                minTexX = 4;minTexY = 5;
                tex1X = 0;tex1Y = 1;
                tex2X = 2;tex2Y = 3;
                ret = DivideResult.TRIFRONT;
            }
            else{
                //min is point 0, is behind
                minX = 0;minY = 1;minZ = 2;
                idx1X = 3;idx1Y = 4;idx1Z = 5;
                idx2X = 6;idx2Y = 7;idx2Z = 8;
                minTexX = 0;minTexY = 1;
                tex1X = 2;tex1Y = 3;
                tex2X = 4;tex2Y = 5;
                ret = DivideResult.QUADFRONT;
            }
        }
        //second front > 0, is min
        else{
            minX = 3;minY = 4;minZ = 5;
            idx1X = 6;idx1Y = 7;idx1Z = 8;
            idx2X = 0;idx2Y = 1;idx2Z = 2;
            minTexX = 2;minTexY = 3;
            tex1X = 4;tex1Y = 5;
            tex2X = 0;tex2Y = 1;
            ret = DivideResult.TRIFRONT;
        }
        vx = tri[idx1X] - tri[minX];
        vy = tri[idx1Y] - tri[minY];
        vz = tri[idx1Z] - tri[minZ];
        t = rightDir[0]*(camPos[0]-tri[minX]) + 
            rightDir[1]*(camPos[1]-tri[minY]) + 
            rightDir[2]*(camPos[2]-tri[minZ]);
        tdiv = t/(rightDir[0]*vx + 
                  rightDir[1]*vy + 
                  rightDir[2]*vz);
        inter1X = tri[minX] + tdiv*vx;
        inter1Y = tri[minY] + tdiv*vy;
        inter1Z = tri[minZ] + tdiv*vz;
        texInter1X = corresponding[minTexX] + 
                     tdiv*(corresponding[tex1X] - corresponding[minTexX]);
        texInter1Y = corresponding[minTexY] + 
                     tdiv*(corresponding[tex1Y] - corresponding[minTexY]);
        if(ret != DivideResult.DOUBLE){
            //second intersection
            vx = tri[idx2X] - tri[minX];
            vy = tri[idx2Y] - tri[minY];
            vz = tri[idx2Z] - tri[minZ];
            tdiv = t/(rightDir[0]*vx + 
                      rightDir[1]*vy + 
                      rightDir[2]*vz);
            inter2X = tri[minX] + tdiv*vx;
            inter2Y = tri[minY] + tdiv*vy;
            inter2Z = tri[minZ] + tdiv*vz;
            texInter2X = corresponding[minTexX] + 
                         tdiv*(corresponding[tex2X] - corresponding[minTexX]);
            texInter2Y = corresponding[minTexY] + 
                         tdiv*(corresponding[tex2Y] - corresponding[minTexY]);
            this.pProj.pointProjectSingle(inter1X, inter1Y, inter1Z, divideProjI1, 0);
            this.pProj.pointProjectSingle(inter2X, inter2Y, inter2Z, divideProjI2, 0);
            //triangle
            divideTri1[0] = tri[minX]; divideTri1[1] = tri[minY]; divideTri1[2] = tri[minZ];
            divideTri1[3] = inter1X; divideTri1[4] = inter1Y; divideTri1[5] = inter1Z;
            divideTri1[6] = inter2X; divideTri1[7] = inter2Y; divideTri1[8] = inter2Z;
            divideTri1Proj[0] = projP[minTexX]; divideTri1Proj[1] = projP[minTexY];
            divideTri1Proj[2] = 0;divideTri1Proj[3] = divideProjI1[1];
            divideTri1Proj[4] = 0;divideTri1Proj[5] = divideProjI2[1];
            divideTex1[0] = corresponding[minTexX]; divideTex1[1] = corresponding[minTexY];
            divideTex1[2] = texInter1X; divideTex1[3] = texInter1Y;
            divideTex1[4] = texInter2X; divideTex1[5] = texInter2Y;

            //quad tri abc
            divideTri2[0] = tri[idx1X]; divideTri2[1] = tri[idx1Y]; divideTri2[2] = tri[idx1Z];
            divideTri2[3] = tri[idx2X]; divideTri2[4] = tri[idx2Y]; divideTri2[5] = tri[idx2Z];
            divideTri2[6] = inter2X; divideTri2[7] = inter2Y; divideTri2[8] = inter2Z;
            divideTri2Proj[0] = projP[tex1X]; divideTri2Proj[1] = projP[tex1Y];
            divideTri2Proj[2] = projP[tex2X]; divideTri2Proj[3] = projP[tex2Y];
            divideTri2Proj[4] = 0; divideTri2Proj[5] = divideProjI2[1];
            divideTex2[0] = corresponding[tex1X]; divideTex2[1] = corresponding[tex1Y];
            divideTex2[2] = corresponding[tex2X]; divideTex2[3] = corresponding[tex2Y];
            divideTex2[4] = texInter2X; divideTex2[5] = texInter2Y;

            //quad tri cda 
            divideTri3[0] = inter2X; divideTri3[1] = inter2Y; divideTri3[2] = inter2Z;
            divideTri3[3] = inter1X; divideTri3[4] = inter1Y; divideTri3[5] = inter1Z;
            divideTri3[6] = tri[idx1X]; divideTri3[7] = tri[idx1Y]; divideTri3[8] = tri[idx1Z];
            divideTri3Proj[0] = 0; divideTri3Proj[1] = divideProjI2[1];
            divideTri3Proj[2] = 0; divideTri3Proj[3] = divideProjI1[1];
            divideTri3Proj[4] = projP[tex1X]; divideTri3Proj[5] = projP[tex1Y];
            divideTex3[0] = texInter2X; divideTex3[1] = texInter2Y;
            divideTex3[2] = texInter1X; divideTex3[3] = texInter1Y;
            divideTex3[4] = corresponding[tex1X]; divideTex3[5] = corresponding[tex1Y];
        }
        else{
            //in double case, min is point on plane, idx1 is point in front,
            //idx2 is point behind, inter1 is intersection between idx1 and idx2 on plane
            //front triangle
            divideTri1[0] = tri[idx1X]; divideTri1[1] = tri[idx1Y]; divideTri1[2] = tri[idx1Z];
            divideTri1[3] = inter1X; divideTri1[4] = inter1Y; divideTri1[5] = inter1Z;
            divideTri1[6] = tri[minX]; divideTri1[7] = tri[minY]; divideTri1[8] = tri[minZ];
            divideTex1[0] = corresponding[tex1X]; divideTex1[1] = corresponding[tex1Y];
            divideTex1[2] = texInter1X; divideTex1[3] = texInter1Y;
            divideTex1[4] = corresponding[minTexX]; divideTex1[5] = corresponding[minTexY];
            //behind triangle
            divideTri2[0] = inter1X; divideTri2[1] = inter1Y; divideTri2[2] = inter1Z;
            divideTri2[3] = tri[idx2X]; divideTri2[4] = tri[idx2Y]; divideTri2[5] = tri[idx2Z];
            divideTri2[6] = tri[minX]; divideTri2[7] = tri[minY]; divideTri2[8] = tri[minZ];
            divideTex2[0] = texInter1X; divideTex2[1] = texInter1Y;
            divideTex2[2] = corresponding[tex2X]; divideTex2[3] = corresponding[tex2Y];
            divideTex2[4] = minTexX; divideTex2[5] = minTexY;
        }
        return ret;
    }
    
    /**
     * Maps textures as of the triangles specified in the various texture map buffers.
     * 
     * @param upper true if this is the upper texture map thread
     */
    private void mapTex(boolean upper){
        //pull back to start of buffers
        FloatBuffer triPtBuffer,projPtBuffer,triTxBuffer,normalsBuffer;
        IntBuffer triTxNumBuffer;
        float[] zBuffer;
        TextureManager textures;
        float[] lightDirTemp = new float[3];
        if(upper){
            zBuffer = this.zBufferUpper;
            triPtBuffer = this.triPtBufferUpper;
            projPtBuffer = this.projPtBufferUpper;
            normalsBuffer = this.triUnitNormalsUpper;
            triTxBuffer = this.triTxBufferUpper;
            triTxNumBuffer = this.triTxNumBufferUpper;
            textures = this.textureManagers[0];
        }
        else{
            zBuffer = this.zBufferLower;
            triPtBuffer = this.triPtBufferLower;
            projPtBuffer = this.projPtBufferLower;
            normalsBuffer = this.triUnitNormalsLower;
            triTxBuffer = this.triTxBufferLower;
            triTxNumBuffer = this.triTxNumBufferLower;
            textures = this.textureManagers[1];
        }
        triPtBuffer.rewind();
        projPtBuffer.rewind();
        triTxBuffer.rewind();
        normalsBuffer.rewind();
        triTxNumBuffer.rewind();
        int prevTexNum = -1;
        int upperBound = upper ? this.xSizeH : 1;
        int lowerBound = upper ? -1 : -this.xSizeH;
        int zBuffIdx,testPx,testPy,texNum,xmin,xmax,ymin,ymax,iterx,itery;
        float approxD,x1,x2,y,texSpace1,texSpace2,invDeterm,nx,ny,nz;
        float intensityBase,intensityV1,intensityV2,intensityV3,totalIntensity;
        float tri1X,tri1Y,tri1Z,tri2X,tri2Y,tri2Z,tri3X,tri3Y,tri3Z;
        float tex1X,tex1Y,texV1X,texV1Y,texV2X,texV2Y;
        float v1x,v1y,v2x,v2y;
        float projP1X,projP1Y,projP2X,projP2Y,projP3X,projP3Y;
        float distSq1,distSq2,distSq3,v1zDiff,v2zDiff;
        //while there is still a triangle to draw
        while(triPtBuffer.position() < triPtBuffer.limit()){
            //pull world space coordinates of one triangle
            tri1X = triPtBuffer.get();
            tri1Y = triPtBuffer.get();
            tri1Z = triPtBuffer.get();
            tri2X = triPtBuffer.get();
            tri2Y = triPtBuffer.get();
            tri2Z = triPtBuffer.get();
            tri3X = triPtBuffer.get();
            tri3Y = triPtBuffer.get();
            tri3Z = triPtBuffer.get();
            
            //pull unit normal
            nx = normalsBuffer.get();
            ny = normalsBuffer.get();
            nz = normalsBuffer.get();
            
            totalIntensity = 0;
            intensityBase = 0;intensityV2 = 0;intensityV3 = 0;
            if(this.computeLighting){
                //compute normal light contributions
                float contribution;
                for(float[] l : this.globalNormalLights){
                    contribution=l[3]*(l[0]*nx+l[1]*ny+l[2]*nz);
                    if(contribution > 1){
                        contribution = 1;
                    }
                    else if(contribution < l[4]){
                        contribution = l[4];
                    }
                    intensityBase+=contribution;
                }
                //compute point light contributions
                intensityV1 = intensityBase; intensityV2 = intensityBase;intensityV3 = intensityBase;
                //compute light intensity, l is of format [x,y,z,Max,Min]
                for(float[] l : this.globalPointLights){
                    lightDirTemp[0] = l[0] - tri1X;
                    lightDirTemp[1] = l[1] - tri1Y;
                    lightDirTemp[2] = l[2] - tri1Z;
                    FastFMath.normalizeVec(lightDirTemp);
                    contribution=l[3]*(lightDirTemp[0]*nx+lightDirTemp[1]*ny+lightDirTemp[2]*nz);
                    if(contribution > 1){
                        contribution = 1;
                    }
                    else if(contribution < l[4]){
                        contribution = l[4];
                    }
                    intensityV1 += contribution;
                    lightDirTemp[0] = l[0] - tri2X;
                    lightDirTemp[1] = l[1] - tri2Y;
                    lightDirTemp[2] = l[2] - tri2Z;
                    FastFMath.normalizeVec(lightDirTemp);
                    contribution=l[3]*(lightDirTemp[0]*nx+lightDirTemp[1]*ny+lightDirTemp[2]*nz);
                    if(contribution > 1){
                        contribution = 1;
                    }
                    else if(contribution < l[4]){
                        contribution = l[4];
                    }
                    intensityV2 += contribution;
                    lightDirTemp[0] = l[0] - tri3X;
                    lightDirTemp[1] = l[1] - tri3Y;
                    lightDirTemp[2] = l[2] - tri3Z;
                    FastFMath.normalizeVec(lightDirTemp);
                    contribution=l[3]*(lightDirTemp[0]*nx+lightDirTemp[1]*ny+lightDirTemp[2]*nz);
                    if(contribution > 1){
                        contribution = 1;
                    }
                    else if(contribution < l[4]){
                        contribution = l[4];
                    }
                    intensityV3 += contribution;
                }
                intensityBase = intensityBase > 1 ? 1 : intensityBase;
                intensityV2 = intensityV2 > 1 ? 1 : intensityV2;
                intensityV2-=intensityV1;
                intensityV3 = intensityV3 > 1 ? 1 : intensityV3;
                intensityV3-=intensityV1;
            }
            else{
                totalIntensity = 1;
            }
            //pull screen space coords
            projP1X = projPtBuffer.get();
            projP1Y = projPtBuffer.get();
            projP2X = projPtBuffer.get();
            projP2Y = projPtBuffer.get();
            projP3X = projPtBuffer.get();
            projP3Y = projPtBuffer.get();
            
            //distances squared for each tri point
            distSq1 = (tri1X-camPosX)*(tri1X-camPosX)+
                      (tri1Y-camPosY)*(tri1Y-camPosY)+
                      (tri1Z-camPosZ)*(tri1Z-camPosZ);
            distSq2 = (tri2X-camPosX)*(tri2X-camPosX)+
                      (tri2Y-camPosY)*(tri2Y-camPosY)+
                      (tri2Z-camPosZ)*(tri2Z-camPosZ);
            distSq3 = (tri3X-camPosX)*(tri3X-camPosX)+
                      (tri3Y-camPosY)*(tri3Y-camPosY)+
                      (tri3Z-camPosZ)*(tri3Z-camPosZ);
            
            //pull corresponding tex coord
            tex1X = triTxBuffer.get();
            tex1Y = triTxBuffer.get();
            texV1X = triTxBuffer.get() - tex1X;
            texV1Y = triTxBuffer.get() - tex1Y;
            texV2X = triTxBuffer.get() - tex1X;
            texV2Y = triTxBuffer.get() - tex1Y;
            
            //pull tex number 
            texNum = triTxNumBuffer.get();
            
            //have texure manager cache some new metadata if texture has changed
            if(texNum!=prevTexNum){
                prevTexNum = texNum;
                textures.setCurrentTexture(texNum);
            }
            //computing bounding region of triangle, also put into buffer?
            xmin = (int) Math.min(Math.min(projP1X, projP2X), projP3X);
            xmax = 1+(int) Math.max(Math.max(projP1X, projP2X), projP3X);
            ymin = (int) Math.min(Math.min(projP1Y, projP2Y), projP3Y);
            ymax = 1+(int) Math.max(Math.max(projP1Y, projP2Y), projP3Y);
            
            //compute screen space basis v1
            v1x = projP2X - projP1X;
            v1y = projP2Y - projP1Y;
            //interpolate depth drop over v1 -- TODO, don't calc all of distSq array
            //compute values needed here only and distsq of origin
            v1zDiff = distSq2 - distSq1;
            //screen space basis v2
            v2x = projP3X - projP1X;
            v2y = projP3Y - projP1Y;
            //depth drop over v2
            v2zDiff = distSq3 - distSq1;
            //figure out iteration direction; revisit this some day to see wtf is going on
            iterx = 1;
            itery = 1;
            if (xmax < xmin) {
                iterx = -1;
            }
            if (ymax < ymin) {
                itery = -1;
            }
            if (xmax > xSizeH - 1) {
                xmax = xSizeH - 1;
            }
            if (ymax > ySizeH) {
                ymax = ySizeH;
            }
            if (xmin < -xSizeH) {
                xmin = -xSizeH;
            }
            if (ymin < -ySizeH + 1) {
                ymin = -ySizeH + 1;
            }
            //inverse determinant of the basis matrix
            invDeterm = 1 / (v1x * v2y - v1y * v2x);
            //premultiply x vector 
            v1x*=invDeterm;
            v2x*=invDeterm;
            //for each point inside bounding box
            //all this needs to be as optimal as possible as it's per frame
            for (testPx = xmin; testPx * iterx <= xmax * iterx; testPx += iterx) {
                if(testPx > upperBound || testPx < lowerBound) continue;
                //precompute components of basis change that only change w.r.t. x
                x2 = (testPx - projP1X)*invDeterm;
                x1 = v2y * x2;
                x2 *= v1y;
                for (testPy = ymin; testPy * itery <= ymax * itery; testPy += itery) {
                    //this.pixConsidered++;
                    zBuffIdx = this.xSizeH + testPx+(this.ySizeH - testPy)*xSize;
                    //finish change of basis calc from true screen space to texture space 
                    y = testPy - projP1Y;
                    texSpace1 = x1 - v2x * y;
                    texSpace2 = v1x * y - x2;
                    //check if point is inside screen space triangle
                    if (texSpace1 + texSpace2 <= 1 & texSpace1 >= 0 & texSpace2 >= 0) {
                        if(this.computeLighting){
                            totalIntensity = intensityBase + 
                                             texSpace1*intensityV2 + 
                                             texSpace2*intensityV3;
                            totalIntensity = totalIntensity > 1 ? 1 : totalIntensity;
                        }
                        //interpolate depth^2 at that point
                        approxD = texSpace1 * v1zDiff + texSpace2 * v2zDiff + distSq1;
                        //do z buffer check with interpolated depth; also check if
                        //"behind" projection plane
                        if (approxD > this.vDistSq & approxD < zBuffer[zBuffIdx]) {
                            //this.pixRendered++;
                            //either use bilinear or nearest neighbor filtering
                            //depending on configuration
                            if(this.bilinearMapping){
                                textures.bilinearInterpPixel(
                                    tex1X + 
                                      texV1X * texSpace1 + 
                                      texV2X * texSpace2,
                                    tex1Y + 
                                      texV1Y * texSpace1 + 
                                      texV2Y * texSpace2,
                                    zBuffIdx<<2,
                                    this.buffer,totalIntensity
                                );
                            }
                            else{
                                //nearest neighbor lol
                                textures.nearestNeighborPixel(
                                    (int)(tex1X + 
                                      texV1X * texSpace1 + 
                                      texV2X * texSpace2),
                                    (int)(tex1Y +  
                                      texV1Y * texSpace1 + 
                                      texV2Y * texSpace2),
                                    zBuffIdx<<2,
                                    this.buffer
                                );
                            }
                            //update z buffer
                            zBuffer[zBuffIdx] = approxD;
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Indicates if a given point should be clipped out of screen
     * @param pts 3 points to test
     * @param cameraDir camera view direction
     * @param cameraLoc camera location in space
     * @return true of the point should be clipped, false otherwise
     */
    private boolean angleClip(float[] pts, float[] cameraDir, float[] cameraLoc){
        //mag = 1
        float px = pts[0] - cameraLoc[0];
        float py = pts[1] - cameraLoc[1];
        float pz = pts[2] - cameraLoc[2];
        float dot = px*cameraDir[0] + 
                    py*cameraDir[1] + 
                    pz*cameraDir[2];
        dot *= FastFMath.invSqrt(px*px+py*py+pz*pz);
        if(dot >= this.FOVCos) return false;
        px = pts[3] - cameraLoc[0];
        py = pts[4] - cameraLoc[1];
        pz = pts[5] - cameraLoc[2];
        dot = px*cameraDir[0] + 
              py*cameraDir[1] + 
              pz*cameraDir[2];
        dot *= FastFMath.invSqrt(px*px+py*py+pz*pz);
        if(dot >= this.FOVCos) return false;
        px = pts[6] - cameraLoc[0];
        py = pts[7] - cameraLoc[1];
        pz = pts[8] - cameraLoc[2];
        dot = px*cameraDir[0] + 
              py*cameraDir[1] + 
              pz*cameraDir[2];
        dot *= FastFMath.invSqrt(px*px+py*py+pz*pz);
        return dot < this.FOVCos;
    }

    /**
     * does a change of basis computation to simultaneously solve the point in
     * triangle problem and do an affine mapping within the confines of a
     * triangle. A result with x and y both above 0 and x+y below 1 is within
     * the triangle, and the values also give the linear interpolations along
     * the texture vectors.
     *
     * @param space the float[] to calculate the transform unto
     * @param x the float x coordinate to transform
     * @param y the float y coordinate to transform 
     * @param v1 the first vector in screen space from a triangle post
     * projection
     * @param v2 the second vector in screen space from a triangle post
     * projection
     */
    private void changeCoord(float[] space, float x, float y, float[] v1, float[] v2) {
        float invDeterm = 1 / (v1[0] * v2[1] - v1[1] * v2[0]);
        space[0] = (v2[1] * x - v2[0] * y)*invDeterm;
        space[1] = (v1[0] * y - v1[1] * x)*invDeterm;
    }
}
