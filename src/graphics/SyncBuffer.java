/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graphics;

import java.nio.ByteBuffer;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritablePixelFormat;

/**
 *
 * @author Christopher
 */
public class SyncBuffer {

    private final ByteBuffer buffer;
    private final int xSize;
    private final int ySize;
    private final byte[] clean;

    public SyncBuffer(int xSize, int ySize) {
        buffer = ByteBuffer.allocate(xSize * ySize * 4);
        this.xSize = xSize;
        this.ySize = ySize;
        clean = new byte[xSize * ySize * 4];
    }

    public void clear() {
        buffer.rewind();
        buffer.put(clean);
        buffer.clear();
    }

//    public void put(byte toPut) {
//        fWriteLock.lock();
//        try {
//            buffer.put(toPut);
//        } finally {
//            fWriteLock.unlock();
//        }
//    }

    public void put(byte toPut, int index) {
        buffer.put(index, toPut);
    }

    public void rewind() {
        buffer.rewind();
    }

    public void copyToWriter(int xStart, int yStart, int xEnd, int yEnd,
        WritablePixelFormat format, PixelWriter writer) {
        buffer.rewind();
        writer.setPixels(xStart, yStart, xEnd, yEnd, format, buffer, (xEnd - xStart) * 4);
    }

    public byte getPixelByte(int x, int y, int channel) {
        return buffer.get(this.xSize * y * 4 + x * 4 + channel);
    }

    public void setPixelByte(int x, int y, int channel, byte value) {
        buffer.put(this.xSize * y * 4 + x * 4 + channel, value);
    }

    public void setPixel(int x, int y, int maxX, byte[] color) {
        buffer.position(x * 4 + y * maxX * 4);
        buffer.put(color);
    }
}
