/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import javafx.scene.image.Image;
import javafx.scene.image.WritablePixelFormat;
import javax.imageio.ImageIO;

/**
 *
 * @author Christopher
 */
public class TextureManager {

    private final ArrayList<byte[]> textureRasters;
    private final WritablePixelFormat format;
    private final HashMap<String, TexMetadata> nameMappings;
    private final HashMap<Integer,String> idMappings;
    private int currentTexNum;
    private byte[] currentRaster;
    private final byte[] bilinScratch;
    private int currMaxX;
    private int currMaxY;

    ///this needs to be fixed so it is platform independent 
    public TextureManager() {
        this.textureRasters = new ArrayList<>();
        format = WritablePixelFormat.getByteBgraPreInstance();
        nameMappings = new HashMap<>();
        idMappings = new HashMap<>();
        this.bilinScratch = new byte[16];
    }

    public TextureManager(File loadAll) {
        this.textureRasters = new ArrayList<>();
        format = WritablePixelFormat.getByteBgraPreInstance();
        nameMappings = new HashMap<>();
        idMappings = new HashMap<>();
        System.out.println("file list: " + Arrays.toString(loadAll.listFiles()));
        for (File f : loadAll.listFiles()) {
            System.out.println(f.getAbsolutePath());
            if (f.isFile() && (f.getName().contains(".bmp") || 
                               f.getName().contains(".jpg") ||
                               f.getName().contains(".png"))) {
                Image img = new Image(f.toURI().toString());
                if (img.getPixelReader() != null) {
                    ByteBuffer buffer = ByteBuffer.allocate((int) img.getWidth()
                            * (int) img.getHeight() * 4);
                    img.getPixelReader().getPixels(0, 0, (int) img.getWidth(), (int) img.getHeight(),
                            format, buffer, (int) img.getWidth() * 4);
                    textureRasters.add(buffer.array());
                    nameMappings.put(f.getName(), 
                                     new TexMetadata((float)img.getWidth(),
                                                     (float)img.getHeight(),
                                                     textureRasters.size()-1));
                    idMappings.put(textureRasters.size()-1, f.getName());
                }
            }
        }
        this.bilinScratch = new byte[16];
    }
    
    /**
     * Constructor for a texture manager that will share the actual texture buffers
     * underneath, but keep track of it's own cached metadata. 
     * @param shared TextureManager to share tex buffers with
     */
    public TextureManager(TextureManager shared){
        this.textureRasters = shared.textureRasters;
        this.format = shared.format;
        this.nameMappings = shared.nameMappings;
        this.idMappings = shared.idMappings;
        this.bilinScratch = new byte[16];
    }
    
    private class TexMetadata{
        
        private final float width;
        private final float height;
        private final int id;
        public TexMetadata(float width, float height, int id){
            this.width = width;
            this.height = height;
            this.id = id;
        }
        
        public float getWidth(){
            return this.width;
        }
        
        public float getHeight(){
            return this.height;
        }
        
        public int getId(){
            return this.id;
        }
    }
    
    public int getTexIndexByName(String name){
        return this.nameMappings.get(name).getId();
    }
    
    public float[] getTexDimByIdFloat(int id){
        return new float[] {
            this.nameMappings.get(this.idMappings.get(id)).getWidth(),
            this.nameMappings.get(this.idMappings.get(id)).getHeight()};
    }
    public int[] getTexDimByIdInt(int id){
        return new int[] {
            (int)this.nameMappings.get(this.idMappings.get(id)).getWidth(),
            (int)this.nameMappings.get(this.idMappings.get(id)).getHeight()};
    }
    
    public int currentTexNum(){
        return this.currentTexNum;
    }
    
    public void setCurrentTexture(int texNum){
        this.currentTexNum = texNum;
        this.currentRaster = this.textureRasters.get(texNum);
        this.currMaxX = (int)this.nameMappings.get(this.idMappings.get(texNum)).getWidth();
        this.currMaxY = (int)this.nameMappings.get(this.idMappings.get(texNum)).getHeight();
    }
    
    /**
     * adds a texture and returns the index of the added texture
     *
     * @param file file pointing to texture to add
     * @return id of the added texture, -1 on failure
     */
    public int addTexture(File file) {
        if (file.isFile() && (file.getName().contains(".bmp") || file.getName().contains(".jpg") ||
            file.getName().contains(".png"))) {
            Image img = new Image(file.toURI().toString());
            if (img.getPixelReader() != null) {
                ByteBuffer buffer = ByteBuffer.allocate((int) img.getWidth()
                        * (int) img.getHeight() * 4);
                img.getPixelReader().getPixels(0, 0, (int) img.getWidth(), (int) img.getHeight(),
                        format, buffer, (int) img.getWidth() * 4);
                textureRasters.add(buffer.array());
                nameMappings.put(file.getName(), 
                                     new TexMetadata((float)img.getWidth(),
                                                     (float)img.getHeight(),
                                                     textureRasters.size()-1));
                idMappings.put(textureRasters.size()-1, file.getName());
                return textureRasters.size()-1;
            }
        }
        return -1;
    }
    
    /**
     * adds a texture and returns the index of the added texture
     *
     * @param loadAll file pointing to texture to add, -1 of failure
     */
    public void addTextures(File loadAll) {
        for (File f : loadAll.listFiles()) {
            System.out.println(f.getAbsolutePath());
            if (f.isFile() && (f.getName().contains(".bmp") || 
                               f.getName().contains(".jpg") ||
                               f.getName().contains(".png"))) {
                Image img = new Image(f.toURI().toString());
                if (img.getPixelReader() != null) {
                    ByteBuffer buffer = ByteBuffer.allocate((int) img.getWidth()
                            * (int) img.getHeight() * 4);
                    img.getPixelReader().getPixels(0, 0, (int) img.getWidth(), (int) img.getHeight(),
                            format, buffer, (int) img.getWidth() * 4);
                    textureRasters.add(buffer.array());
                    nameMappings.put(f.getName(), 
                                         new TexMetadata((float)img.getWidth(),
                                                         (float)img.getHeight(),
                                                         textureRasters.size()-1));
                    idMappings.put(textureRasters.size()-1, f.getName());
                }
            }
        }
    }
    
    /**
     * adds a texture and returns the index of the added texture
     *
     * @param file file pointing to texture to add, -1 of failure
     * @return id of added texture, -1 on failure
     */
    public int addTextureBufferedImage(File file) {
        if (file.isFile() && (file.getName().contains(".bmp") || file.getName().contains(".jpg") ||
            file.getName().contains(".png"))) {
            BufferedImage in;
                try {
                    in = ImageIO.read(file);
                } catch (IOException ex) {
                    return -1;
                }
                BufferedImage img = new BufferedImage(
                    in.getWidth(), in.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
                Graphics2D g = img.createGraphics();
                g.drawImage(in, 0, 0, null);
                g.dispose();
                byte[] arr = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
                textureRasters.add(arr);
                nameMappings.put(file.getName(), 
                                 new TexMetadata((float)img.getWidth(),
                                                 (float)img.getHeight(),
                                                 textureRasters.size()-1));
                idMappings.put(textureRasters.size()-1, file.getName());
                return textureRasters.size()-1;
        }
        return -1;
    }
    
    public void addTexturesBufferedImage(File loadAll){
        for (File f : loadAll.listFiles()) {
            System.out.println(f.getAbsolutePath());
            if (f.isFile() && (f.getName().contains(".bmp") || 
                               f.getName().contains(".jpg") ||
                               f.getName().contains(".png"))) {
                BufferedImage in;
                try {
                    in = ImageIO.read(f);
                } catch (IOException ex) {
                    continue;
                }
                BufferedImage img = new BufferedImage(
                    in.getWidth(), in.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
                Graphics2D g = img.createGraphics();
                g.drawImage(in, 0, 0, null);
                g.dispose();
                byte[] arr = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
                textureRasters.add(arr);
                nameMappings.put(f.getName(), 
                                 new TexMetadata((float)img.getWidth(),
                                                 (float)img.getHeight(),
                                                 textureRasters.size()-1));
                idMappings.put(textureRasters.size()-1, f.getName());
            }
        }
    }
    
    public final void nearestNeighborPixel(int x, int y, int offset, byte[] buffer) {
        int pos =  4*(x + y * currMaxX);
        if(pos >= currentRaster.length-4 | offset >= buffer.length-4){
            return;
        }
        System.arraycopy(currentRaster, pos, buffer, offset, 4);
    }
    
    public final void bilinearInterpPixel(float x, float y, int offset, byte[] buffer,float intensity){
        //should do something in case of out of bounds request
        if(x+1 >= currMaxX )x = currMaxX - 2;
        if(y+1 >= currMaxY )y = currMaxY - 2;
        int pos = 4*((int)(x) + ((int)(y))* currMaxX);
        if(pos >= currentRaster.length-8){
            buffer[0] = (byte)255;
            buffer[1] = (byte)0;
            buffer[2] = (byte)0;
            buffer[3] = (byte)255;
            return;
        }
        float scalex = x - (int)x;
        float scaley = y - (int)y;
        //enforce bounds
        float scalebl = (1-scalex)*(1-scaley);
        scalebl = scalebl > 1f ? 1f : scalebl;
        float scalebr = scalex*(1-scaley);
        scalebr = scalebr > 1f ? 1f : scalebr;
        float scaletr = scalex*scaley;
        scaletr = scaletr > 1f ? 1f : scaletr;
        float scaletl = (1-scalex)*scaley;
        scaletl = scaletl > 1f ? 1f : scaletl;
        //copy tl and tr
        System.arraycopy(this.currentRaster, pos, bilinScratch, 0, 8);
        //copy bl and br; add one to y value and load 2 pixels there; if we 
        //are on bottom row, copy 0's
        if(y > currMaxY - 2){
            Arrays.fill(bilinScratch, 8, 8, (byte)0);
        }
        else{
            System.arraycopy(this.currentRaster, pos+4*currMaxX, bilinScratch, 8, 8);
        }
        //due to the lack of an unsigned byte in java this is obnoxious
        buffer[offset] = (byte)(intensity*(((int)bilinScratch[0] & 0xFF)*scalebl+
                                           ((int)bilinScratch[4] & 0xFF)*scalebr+
                                           ((int)bilinScratch[8] & 0xFF)*scaletl+
                                           ((int)bilinScratch[12] & 0xFF)*scaletr));
        buffer[offset+1] = (byte)(intensity*(((int)bilinScratch[1] & 0xFF)*scalebl+
                                             ((int)bilinScratch[5] & 0xFF)*scalebr+
                                             ((int)bilinScratch[9] & 0xFF)*scaletl+
                                             ((int)bilinScratch[13] & 0xFF)*scaletr));
        buffer[offset+2] = (byte)(intensity*(((int)bilinScratch[2] & 0xFF)*scalebl+
                                             ((int)bilinScratch[6] & 0xFF)*scalebr+
                                             ((int)bilinScratch[10] & 0xFF)*scaletl+
                                             ((int)bilinScratch[14] & 0xFF)*scaletr));
        //TODO interp this too
        buffer[offset+3] = (byte)255;
    }
}
